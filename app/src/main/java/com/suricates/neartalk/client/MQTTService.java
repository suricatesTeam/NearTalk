package com.suricates.neartalk.client;

import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.os.RemoteException;

import com.suricates.neartalk.R;
import com.suricates.neartalk.client.models.Message;
import com.suricates.neartalk.http.AsyncTaskManager;
import com.suricates.neartalk.utils.Const;
import com.suricates.neartalk.utils.GoogleClient;
import com.suricates.neartalk.utils.LocationManager;
import com.suricates.neartalk.utils.Preferences;
import com.suricates.neartalk.utils.SLog;
import com.suricates.neartalk.utils.U;
import com.suricates.neartalk.utils.DBHelper;

import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * Service class to send and receive the messages, included if the app is closed (if specified in the preferences)
 */
public class MQTTService extends Service implements GlobalClient.Callback {
	
	private GlobalClient mGlobalClient;
	
	private final Messenger clientToServiceMsg = new Messenger(new IncomingHandler(this));
	private Messenger replyTo;
	
	private final DBHelper mDBHelper = new DBHelper(this);
	private String mCurrentZone;
	
	/**
	 * Handle the incoming messages
	 */
	private static class IncomingHandler extends Handler {
		private final MQTTService mqttService;
		
		IncomingHandler(MQTTService mqttService) {
			this.mqttService = mqttService;
		}
		
		@Override
		public void handleMessage(android.os.Message msg) {
			SLog.d("Message received, what: " + msg.what);
			switch (msg.what) {
				case Const.mqtt.SERVICE_OUTGOING_MESSAGE_WHAT:
					Bundle data = msg.getData();
					if (data != null) {
						Message outMessage = (Message) data.getSerializable(Const.mqtt.SERVICE_OUTGOING_MESSAGE_BUNDLE_KEY);
						mqttService.sendMessage(outMessage);
					} else {
						SLog.e("Couldn't get the data from the outgoing message");
					}
					break;
				case Const.mqtt.SERVICE_BINDER_MESSAGE_WHAT:
					if (mqttService.mGlobalClient == null || !mqttService.mGlobalClient.isConnected()) {
						mqttService.connectGlobalListener();
						LocationManager.instance(mqttService);
					}
					if (msg.replyTo != null) {
						mqttService.setClientMessenger(msg.replyTo);
					}
					if (Preferences.getBoolean(mqttService, Const.sp.IS_GPS_ENABLED, false)
						&& !Preferences.getString(mqttService, Const.sp.CURRENT_ZONE, "").isEmpty()) {
						mqttService.sendServiceConnectedMessage();
						SLog.e("Service connect sent");
					} else {
						if (!mqttService.isSomeGpsEnabled()) {
							mqttService.sendNoGpsMessage();
						} else if (!U.hasInternet(mqttService)) {
							mqttService.sendNoNetworkMessage();
						} else {
							mqttService.sendWaitingForGpsMessage();
						}
					}
					break;
				case Const.mqtt.SERVICE_CHANGE_ZONE_WHAT:
					if (mqttService.mGlobalClient != null) {
						new AsyncTaskManager(mqttService).disconnectUser();
						mqttService.mGlobalClient.unregisterResources();
						mqttService.mGlobalClient.close();
						mqttService.mGlobalClient = null;
					}
					mqttService.connectGlobalListener();
					break;
				default:
					super.handleMessage(msg);
			}
		}
	}
	
	/**
	 * Where to send the messages (a fragment, or activity)
	 *
	 * @param replyTo to whom to send the messages
	 */
	private void setClientMessenger(Messenger replyTo) {
		this.replyTo = replyTo;
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return clientToServiceMsg.getBinder();
	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		if (!Preferences.getBoolean(this, getString(R.string.global_chat_listener_key), true)) { //Should listen
			stopSelf();
		}
		return super.onUnbind(intent);
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		registerReceiver(mBroadcastReceiver, new IntentFilter() {{
			addAction(Const.actions.LOC_MODE_CHANGED);
			addAction(Const.actions.STOP_SERVICE);
			addAction(ConnectivityManager.CONNECTIVITY_ACTION);
		}});
		
		checkGpsEnabled();
		checkNetworkEnabled();
		
		LocationManager.instance(this);
		
		boolean shouldListen = Preferences.getBoolean(this, getString(R.string.global_chat_listener_key), true);
		
		if (intent != null) {
			connectGlobalListener();
			return START_STICKY;
		} else if (shouldListen) { //The service is restarted
			SLog.d("Restarted and should listen");
			connectGlobalListener();
			return START_STICKY;
		}
		
		SLog.i("Service should listen is false, stopping");
		try {
			String saved_zone = Preferences.getString(this, Const.sp.CURRENT_ZONE, "");
			if (mGlobalClient != null) {
				mGlobalClient.unsubscribe(saved_zone);
				new AsyncTaskManager(this).disconnectUser();
				U.close(this);
				mDBHelper.deleteOldZoneMessages();
			}
		} catch (MqttException e) {
			e.printStackTrace();
		}
		stopSelf();
		return START_NOT_STICKY;
	}
	
	@Override
	public void onDestroy() {
		U.silentlyUnregisterReceiver(this, mBroadcastReceiver);
		
		new AsyncTaskManager(this).disconnectUser();
		if (mGlobalClient != null) {
			SLog.d("Disconnecting global client...");
			mGlobalClient.unregisterResources();
			mGlobalClient.close();
			mGlobalClient = null;
			sendServiceStoppedMessage();
			U.close(this);
		}
		if (Preferences.getBoolean(this, getString(R.string.global_chat_listener_key), true)) {
			mDBHelper.deleteOldZoneMessages();
		}
		GoogleClient.destroy();
		LocationManager.destroy();
		U.cancelAllNotifications(this);
		super.onDestroy();
	}
	
	@Override
	public void messageReceived(String topic, Message inMessage) {
		if (replyTo != null) {
			android.os.Message sentMessage = android.os.Message.obtain();
			Bundle data = new Bundle();
			data.putSerializable(Const.mqtt.SERVICE_INCOMING_MESSAGE_BUNDLE_KEY, inMessage);
			sentMessage.setData(data);
			sentMessage.what = Const.mqtt.SERVICE_INCOMING_MESSAGE_WHAT;
			try {
				replyTo.send(sentMessage);
			} catch (RemoteException e) {
				if (e instanceof DeadObjectException) {
					SLog.d("replyTo should be null if the activity is dead! Making it null");
					replyTo = null;
				} else {
					e.printStackTrace();
				}
			}
		}
		if (!inMessage.getClientID().equalsIgnoreCase(
			Preferences.getString(this, Const.sp.USER_ID, null))) {
			mDBHelper.addMessageToDatabase(inMessage);
			U.pushNotification(this);
		} else {
			mDBHelper.setMessageSent(inMessage);
		}
		
		SLog.i("arrived:" + inMessage.toString());
	}
	
	@Override
	public void hasBeenDelivered(Message message) {
		SLog.i("Message delivered: " + message.toString());
	}
	
	@Override
	public void connectSuccess() {
		if (replyTo == null) {
			sendBroadcast(new Intent(Const.actions.REBIND_SERVICE));
		}
		sendServiceConnectedMessage();
		
		String saved_zone = Preferences.getString(MQTTService.this, Const.sp.CURRENT_ZONE, "");
		if (mCurrentZone == null) {
			mCurrentZone = saved_zone;
		}
		if (!mCurrentZone.equals(saved_zone)) {
			try {
				mGlobalClient.unsubscribe(mCurrentZone);
				mCurrentZone = saved_zone;
			} catch (MqttException ignored) {
				SLog.e("Can't unsubscribe: " + ignored.getLocalizedMessage());
			}
		}
		//noinspection Convert2streamapi
		for (Message m : new DBHelper(this).getNotSentMessages()) {
			sendMessage(m);
		}
		SLog.e("Connect success");
	}
	
	@Override
	public void connectComplete() {
		SLog.e("Connect complete");
	}
	
	/**
	 * If there's o GPS service enabled, sends a notification telling that is needed to run the app
	 */
	private void checkGpsEnabled() {
		if (!isSomeGpsEnabled()) {
			U.sendAlertNotification(MQTTService.this, Const.notification.ID_NO_GPS, this.getString(R.string.no_gps_available));
			Preferences.set(MQTTService.this, Const.sp.IS_GPS_ENABLED, false);
			sendNoGpsMessage();
		}
	}
	
	/**
	 * Checks if the phone has any GPS service enabled
	 *
	 * @return true if the service is enabled, false otherwise
	 */
	private boolean isSomeGpsEnabled() {
		android.location.LocationManager locationManager = (android.location.LocationManager) MQTTService.this.getSystemService(Context.LOCATION_SERVICE);
		boolean isGpsEnabled = locationManager.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);
		boolean isGpsNetworkEnabled = locationManager.isProviderEnabled(android.location.LocationManager.NETWORK_PROVIDER);
		Preferences.set(MQTTService.this, Const.sp.IS_GPS_ENABLED, isGpsEnabled || isGpsNetworkEnabled);
		return isGpsEnabled || isGpsNetworkEnabled;
	}
	
	/**
	 * If the network isn't enabled, sends a notification telling that is needed to run the app
	 */
	private void checkNetworkEnabled() {
		if (!U.hasInternet(this)) {
			U.sendAlertNotification(MQTTService.this, Const.notification.ID_NO_NETWORK, this.getString(R.string.no_network_available));
			Preferences.set(MQTTService.this, Const.sp.IS_NETWORK_ENABLED, false);
			sendNoNetworkMessage();
		}
	}
	
	/**
	 * Checks whether the mobile data is enabled or not
	 *
	 * @return true if it is enabled, false otherwise
	 */
	private boolean isMobileDataOn() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo nInfo = cm.getActiveNetworkInfo();
		return nInfo != null && nInfo.getType() == ConnectivityManager.TYPE_MOBILE;
	}
	
	/**
	 * Connects the MQTTClient
	 */
	public void connectGlobalListener() {
		if (!U.hasInternet(this)) {
			sendNoNetworkMessage();
			return;
		}
		if (!isSomeGpsEnabled()) {
			sendNoGpsMessage();
			return;
		}
		
		String saved_zone = Preferences.getString(this, Const.sp.CURRENT_ZONE, "");
		if (!saved_zone.isEmpty()) {
			connectGlobalListener(saved_zone);
		} else {
			SLog.e("No zone defined, can't connect listener!");
			sendWaitingForGpsMessage();
			new CountDownTimer(5000, 500) {
				String saved_zone;
				boolean done = false;
				
				@Override
				public void onTick(long millisUntilFinished) {
					if (!done) {
						saved_zone = Preferences.getString(MQTTService.this, Const.sp.CURRENT_ZONE, "");
						if (!saved_zone.isEmpty()) {
							done = true;
							connectGlobalListener(saved_zone);
						}
					}
				}
				
				@Override
				public void onFinish() {
					if (!done) {
						this.start();
					}
				}
			}.start();
		}
	}
	
	/**
	 * Notifies the client that it has been successfully connected to the zone
	 */
	private void sendServiceConnectedMessage() {
		if (replyTo != null && mGlobalClient != null && mGlobalClient.isConnected()) {
			android.os.Message sentMessage = android.os.Message.obtain();
			sentMessage.what = Const.mqtt.SERVICE_GLOBAL_CONNECTED_WHAT;
			try {
				replyTo.send(sentMessage);
			} catch (RemoteException e) {
				if (e instanceof DeadObjectException) {
					SLog.d("replyTo should be null if the activity is dead! Making it null");
					replyTo = null;
				} else {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Used to tell the app that there is no gps
	 */
	private void sendNoGpsMessage() {
		if (replyTo != null) {
			android.os.Message sentMessage = android.os.Message.obtain();
			sentMessage.what = Const.mqtt.SERVICE_NO_GPS_WHAT;
			try {
				replyTo.send(sentMessage);
			} catch (RemoteException e) {
				if (e instanceof DeadObjectException) {
					SLog.d("replyTo should be null if the activity is dead! Making it null");
					replyTo = null;
				} else {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Used to tell the app that there is no gps
	 */
	private void sendWaitingForGpsMessage() {
		if (replyTo != null) {
			android.os.Message sentMessage = android.os.Message.obtain();
			sentMessage.what = Const.mqtt.SERVICE_WAITING_GPS_WHAT;
			try {
				replyTo.send(sentMessage);
			} catch (RemoteException e) {
				if (e instanceof DeadObjectException) {
					SLog.d("replyTo should be null if the activity is dead! Making it null");
					replyTo = null;
				} else {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Used to tell the app that there is no network
	 */
	private void sendNoNetworkMessage() {
		if (replyTo != null) {
			android.os.Message sentMessage = android.os.Message.obtain();
			sentMessage.what = Const.mqtt.SERVICE_NO_NETWORK_WHAT;
			try {
				replyTo.send(sentMessage);
			} catch (RemoteException e) {
				if (e instanceof DeadObjectException) {
					SLog.d("replyTo should be null if the activity is dead! Making it null");
					replyTo = null;
				} else {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Used to tell the app that the service is not running
	 */
	private void sendServiceStoppedMessage() {
		if (replyTo != null) {
			android.os.Message sentMessage = android.os.Message.obtain();
			sentMessage.what = Const.mqtt.SERVICE_GLOBAL_STOPPED_WHAT;
			try {
				replyTo.send(sentMessage);
			} catch (RemoteException e) {
				if (e instanceof DeadObjectException) {
					SLog.d("replyTo should be null if the activity is dead! Making it null");
					replyTo = null;
				} else {
					e.printStackTrace();
				}
			}
		}
	}
	
	/**
	 * Connects the MQTTClient
	 *
	 * @param zone_name the users current zone
	 */
	public void connectGlobalListener(String zone_name) {
		LocationManager.instance(this);
		
		if (mGlobalClient == null) {
			try {
				final String mCurrentClientID = Preferences.getString(
					this,
					Const.sp.USER_ID,
					"nt-" + System.currentTimeMillis() / 1000
				);
				mGlobalClient = new GlobalClient(
					this,
					Const.mqtt.TCP,
					mCurrentClientID,
					GlobalClient.Ack.AUTO_ACK,
					zone_name
				);
				mGlobalClient.setCallback(this);
				mDBHelper.createZoneDatabase();
			} catch (Exception e) {
				e.printStackTrace();
				SLog.e("Couldn't connect with the server. e: " + e);
			}
		} else {
			SLog.e("mGlobalClient is not null!");
			sendServiceConnectedMessage();
		}
	}
	
	/**
	 * Sends a message to the server
	 *
	 * @param message the message to send
	 */
	public void sendMessage(Message message) {
		if (mGlobalClient != null) {
			mGlobalClient.sendMessage(message);
		}
	}
	
	/**
	 * Interface used to know when the app has received a message
	 */
	public interface OnMessageFromServiceReceivedListener {
		
		/**
		 * Message received
		 *
		 * @param message the message received
		 */
		void messageFromServiceReceived(Message message);
	}
	
	private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			switch (intent.getAction()) {
				case Const.actions.LOC_MODE_CHANGED: {
					if (!isSomeGpsEnabled()) {
						U.sendAlertNotification(MQTTService.this, Const.notification.ID_NO_GPS, MQTTService.this.getString(R.string.no_gps_available));
						sendNoGpsMessage();
						String saved_zone = Preferences.getString(MQTTService.this, Const.sp.CURRENT_ZONE, "");
						try {
							if (mGlobalClient != null) mGlobalClient.unsubscribe(saved_zone);
						} catch (MqttException ignored) {
						}
						U.close(MQTTService.this);
					} else {
						NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
						notificationManager.cancel(Const.notification.ID_NO_GPS);
						connectGlobalListener();
					}
				}
				break;
				case Const.actions.STOP_SERVICE: {
					NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
					notificationManager.cancel(Const.notification.ID_NO_GPS);
					sendServiceStoppedMessage();
					MQTTService.this.stopSelf();
				}
				break;
				case ConnectivityManager.CONNECTIVITY_ACTION: {
					if (U.hasInternet(MQTTService.this)) {
						SLog.i("CONNECTED");
						NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
						notificationManager.cancel(Const.notification.ID_NO_NETWORK);
						Preferences.set(MQTTService.this, Const.sp.IS_NETWORK_ENABLED, true);
						connectGlobalListener();
					} else if (!isMobileDataOn()) {
						SLog.i("DISCONNECTED");
						U.sendAlertNotification(MQTTService.this, Const.notification.ID_NO_NETWORK, MQTTService.this.getString(R.string.no_network_available));
						Preferences.set(MQTTService.this, Const.sp.IS_NETWORK_ENABLED, false);
						sendNoNetworkMessage();
					}
				}
				break;
				default:
					SLog.e("Don't know what to do with action: " + intent.getAction());
					break;
			}
		}
	};
}
