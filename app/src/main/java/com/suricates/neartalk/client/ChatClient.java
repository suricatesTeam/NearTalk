package com.suricates.neartalk.client;

import android.content.Context;
import android.widget.Toast;

import com.suricates.neartalk.R;
import com.suricates.neartalk.client.models.Message;
import com.suricates.neartalk.utils.Const;
import com.suricates.neartalk.utils.Preferences;
import com.suricates.neartalk.utils.SLog;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

/**
 * MQTTClient used to send the messages, to as much topic the user wants
 */
public class ChatClient extends MqttAndroidClient implements MqttCallbackExtended {
	
	private final MqttConnectOptions options;
	private String mTopic;
	private int mReconnectTries = 0;
	
	/**
	 * Connects the client, and after the connect is successful, subscribes to the topic
	 *
	 * @param topic the topic to subscribe to
	 */
	public void connectWithSubscribe(String topic) {
		mTopic = topic;
		try {
			connect(options, new IMqttActionListener() {
				@Override
				public void onSuccess(IMqttToken asyncActionToken) {
					SLog.i("ChatClient connection success!");
					mListener.connectSuccess();
					subscribe(mTopic);
				}
				
				@Override
				public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
					exception.printStackTrace();
					SLog.e("ChatClient connection failed");
					reconnect();
				}
			});
		} catch (MqttException e) {
			e.printStackTrace();
			reconnect();
		}
	}
	
	/**
	 * The quality of the service.
	 * The different values are: LOW, MEDIUM, HIGH. MEDIUM by default.
	 */
	private enum Quality {
		LOW(0), MEDIUM(1), HIGH(2);
		
		private int qos;
		
		Quality(int qos) {
			this.qos = qos;
		}
		
		public int getQos() {
			return qos;
		}
	}
	
	private static final String mServerTopic = Const.mqtt.TOPIC;
	
	private Context mContext;
	private Callback mListener;
	
	/**
	 * Creates a new Mqtt client
	 *
	 * @param context     The application context
	 * @param serverURI   The server URI (protocol, name and host)
	 * @param clientId    The name by which this connection should be identified by the server
	 * @param acknowledge The client acknowledge mode, on how should treat the connection.
	 *                    AUTO_ACK -> As soon as the message arrives it is marked as received.
	 *                    MANUAL_ACK -> Yo must tell the client manually if it has been received or not.
	 */
	public ChatClient(Context context, String serverURI, String clientId, Ack acknowledge) throws Exception {
		super(context, serverURI, clientId, acknowledge);
		
		mContext = context;
		options = new MqttConnectOptions();
		options.setAutomaticReconnect(true);
		options.setCleanSession(false);
	}
	
	/**
	 * Sets a Callback for this Client.
	 *
	 * @param callback The callback for the messages
	 */
	public void setCallback(Callback callback) {
		setCallback(this);
		mListener = callback;
	}
	
	/**
	 * Subscribe this client to the specified topic, with the default quality value see {@link Quality}.
	 *
	 * @param topic The topic to subscribe.
	 * @return An IMqttToken instance, or null if could not be subscribed.
	 */
	public IMqttToken subscribe(String topic) {
		try {
			return subscribe(topic, Quality.MEDIUM.getQos());
		} catch (MqttException e) {
			e.printStackTrace();
			SLog.e("Couldn't subscribe to the topic: " + topic);
		}
		return null;
	}
	
	/**
	 * Subscribe this client to the specified topic, with the default quality value see {@link Quality}.
	 *
	 * @param topic   The topic to subscribe.
	 * @param quality The quality of the service.
	 * @return An IMqttToken instance, or null if could not be subscribed.
	 */
	public IMqttToken subscribe(String topic, Quality quality) {
		try {
			return subscribe(topic, quality.getQos());
		} catch (MqttException e) {
			e.printStackTrace();
			SLog.e("Couldn't subscribe to the topic: " + topic);
		}
		return null;
	}
	
	/**
	 * Subscribe to multiple topics, with the specified quality of service.
	 *
	 * @param quality The quality of the service.
	 * @param topics  The topics to subscribe to.
	 * @return An IMqttToken instance, or null if t could not be subscribe to some of them.
	 */
	public IMqttToken subscribe(Quality quality, String... topics) {
		try {
			int qos = quality.getQos();
			for (String topic : topics) {
				subscribe(topic, qos);
			}
		} catch (MqttException e) {
			e.printStackTrace();
			SLog.e("Couldn't connect to one of the topics. ERROR: " + e.getMessage());
		}
		return null;
	}
	
	/**
	 * Send a message to the specified topic.
	 *
	 * @param message The message to send.
	 * @return An IMqttDeliveryToken instance, or null if it could not be sent.
	 */
	public IMqttDeliveryToken sendMessage(Message message) {
		try {
			return publish(mServerTopic, message.getJsonPayload(), Quality.MEDIUM.getQos(), false);
		} catch (MqttException e) {
			e.printStackTrace();
			SLog.e("Couldn't send the message. ERROR: " + e.getMessage());
		}
		return null;
	}
	
	/**
	 * Send a message to the specified topic.
	 *
	 * @param message The message to send.
	 * @param quality The quality of the service, see {@link Quality}
	 * @return An IMqttDeliveryToken instance, or null if it could not be sent.
	 */
	public IMqttDeliveryToken sendMessage(Message message, Quality quality) {
		try {
			return publish(mServerTopic, message.getJsonPayload(), quality.getQos(), false);
		} catch (MqttException e) {
			e.printStackTrace();
			SLog.e("Couldn't send the message. ERROR: " + e.getMessage());
		}
		return null;
	}
	
	/**
	 * Send a message to the specified topic.
	 *
	 * @param message  The message to send.
	 * @param quality  The quality of the service, see {@link Quality}
	 * @param retained If the message should be retained by the server.
	 * @return An IMqttDeliveryToken instance, or null if it could not be sent.
	 */
	public IMqttDeliveryToken sendMessage(Message message, Quality quality, boolean retained) {
		try {
			return publish(mServerTopic, message.getJsonPayload(), quality.getQos(), retained);
		} catch (MqttException e) {
			e.printStackTrace();
			SLog.e("Couldn't send the message. ERROR: " + e.getMessage());
		}
		return null;
	}
	
	@Override
	public void close() {
		mListener = null;
		super.close();
	}
	
	@Override
	public void connectComplete(boolean reconnect, String serverURI) {
		mReconnectTries = 0;
		mListener.connectComplete();
		if (reconnect) {
			SLog.i("Reconnected to the server {URI: '" + serverURI + "'}");
			return;
		}
		SLog.i("Connected to the server {URI: '" + serverURI + "'}");
	}
	
	@Override
	public void connectionLost(Throwable cause) {
		Toast.makeText(mContext, R.string.connection_lost, Toast.LENGTH_SHORT).show();
		if (cause == null) SLog.w("Connection with the server lost. Cause: null");
		else SLog.w("Connection with the server lost. Cause: " + cause.getMessage());
		
		connectWithSubscribe(mTopic);
	}
	
	@Override
	public void messageArrived(String topic, MqttMessage message) {
		try {
			mListener.messageReceived(topic, Message.fromJsonPayload(message.getPayload()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void deliveryComplete(IMqttDeliveryToken token) {
		try {
			mListener.hasBeenDelivered(Message.fromJsonPayload(token.getMessage().getPayload()));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Tries to reconnect the client, if it is not successful after 10 tries, waits for a while and tries again
	 */
	private void reconnect() {
		boolean shouldListen = Preferences.getBoolean(mContext, mContext.getString(R.string.global_chat_listener_key), true);
		if (shouldListen) {
			try {
				mReconnectTries++;
				if (mReconnectTries < 10) Thread.sleep(2000);
				else Thread.sleep(20000);
			} catch (InterruptedException ignored) {
			}
			SLog.e("Reconnecting...");
			connectWithSubscribe(mTopic);
		}
	}
	
	/**
	 * Interface to manage the incoming messages,
	 * without needing to implement the full {@link MqttCallback} interface.
	 */
	interface Callback {
		/**
		 * A new message has arrived.
		 *
		 * @param topic   The topic where the message arrived.
		 * @param message The message object.
		 */
		void messageReceived(String topic, Message message);
		
		/**
		 * Notify the message you sent, has been delivered to the network.
		 *
		 * @param message The message which was sent
		 */
		void hasBeenDelivered(Message message);
		
		/**
		 * Notify when the connection to the broker has succeeded.
		 */
		void connectSuccess();
		
		/**
		 * Notify when the connection to the broker has completed.
		 */
		void connectComplete();
	}
}
