package com.suricates.neartalk.client;

import android.content.Context;

import com.suricates.neartalk.client.models.Message;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttException;

/**
 * MQTTClient to send the messages, but to just one topic
 */
public class GlobalClient extends ChatClient {
	
	private final String zoneID;
	
	/**
	 * Creates a new Mqtt client
	 *
	 * @param context     The application context
	 * @param serverURI   The server URI (protocol, name and host)
	 * @param clientId    The name by which this connection should be identified by the server
	 * @param acknowledge The client acknowledge mode, on how should treat the connection.
	 *                    AUTO_ACK -> As soon as the message arrives it is marked as received.
	 */
	public GlobalClient(Context context, String serverURI, String clientId, Ack acknowledge, String topic) throws Exception {
		super(context, serverURI, clientId, acknowledge);
		super.connectWithSubscribe(topic);
		
		zoneID = topic;
	}
	
	/**
	 * Don't need to subscribe to anything, it is done automatically.
	 *
	 * @param topic The topic to subscribe.
	 * @param qos   The quality of service
	 * @return An IMqttToken instance, or null if it could not subscribe to the topic.
	 * @throws MqttException if it can't subscribe to the topic
	 */
	@Deprecated
	@Override
	public IMqttToken subscribe(String topic, int qos) throws MqttException {
		return super.subscribe(topic, qos);
	}
	
	/**
	 * Sends a message to the global chat
	 *
	 * @param message The message to send.
	 * @return An IMqttDeliveryToken instance
	 */
	public IMqttDeliveryToken sendMessage(Message message) {
		message.setZoneID(zoneID);
		return super.sendMessage(message);
	}
}