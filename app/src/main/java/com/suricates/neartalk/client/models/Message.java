package com.suricates.neartalk.client.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.UUID;

/**
 * POJO class of the message sent to the server
 */
public class Message implements Serializable {
	
	private static final long serialVersionUID = 123456789L;
	
	private String origin;
	private String destination;
	private String message;
	private long timestamp;
	private String clientID;
	private String zoneID;
	private String messageID;
	private boolean sent;
	
	private Message() {
	}
	
	public Message(String origin, String destination, String clientID, String message, long timestamp) {
		this.origin = origin;
		this.destination = destination;
		this.message = message;
		this.timestamp = timestamp;
		this.clientID = clientID;
		this.sent = false;
	}
	
	public Message(String origin, String destination, String message, String messageID, long timestamp, String clientID, String zoneID, boolean sent) {
		this.origin = origin;
		this.destination = destination;
		this.message = message;
		this.messageID = messageID;
		this.timestamp = timestamp;
		this.clientID = clientID;
		this.zoneID = zoneID;
		this.sent = sent;
	}
	
	public String getOrigin() {
		return origin;
	}
	
	public void setOrigin(String origin) {
		this.origin = origin;
	}
	
	public String getDestination() {
		return destination;
	}
	
	public void setDestination(String destination) {
		this.destination = destination;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public long getTimestamp() {
		return timestamp;
	}
	
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	
	public String getClientID() {
		return clientID;
	}
	
	public void setClientID(String clientID) {
		this.clientID = clientID;
	}
	
	public String getZoneID() {
		return zoneID;
	}
	
	public void setZoneID(String zoneID) {
		this.zoneID = zoneID;
	}
	
	public String getMessageID() {
		return messageID;
	}
	
	public void setMessageID(String messageID) {
		this.messageID = messageID;
	}
	
	public boolean isSent() {
		return sent;
	}
	
	public void setSent(boolean sent) {
		this.sent = sent;
	}
	
	/**
	 * Gets an instance of a Message object from an array of bytes, which will be provided by an MqttMessage object.
	 *
	 * @param bytes The array of bytes.
	 * @return A com.suricates.neartalk.client.models.Message instance.
	 */
	public static Message fromJsonPayload(byte[] bytes) throws JSONException {
		JSONObject parsed = new JSONObject(new String(bytes));
		Message message = new Message();
		message.setOrigin(parsed.getString("origin"));
		message.setDestination(parsed.getString("destination"));
		message.setMessage(parsed.getString("message"));
		message.setTimestamp(parsed.getLong("timestamp"));
		message.setClientID(parsed.getString("client_id"));
		message.setZoneID(parsed.getString("zone_id"));
		message.setMessageID(parsed.getString("message_id"));
		message.setSent(true);
		return message;
	}
	
	/**
	 * Gets the Message as an array of bytes.
	 *
	 * @return The array of bytes.
	 */
	public byte[] getJsonPayload() {
		try {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("origin", origin != null && !origin.isEmpty() ? origin : null);
			jsonObject.put("destination", destination != null && !destination.isEmpty() ? destination : null);
			jsonObject.put("message", message != null && !message.isEmpty() ? message : null);
			jsonObject.put("timestamp", timestamp);
			jsonObject.put("client_id", clientID != null && !clientID.isEmpty() ? clientID : null);
			jsonObject.put("zone_id", zoneID != null && !zoneID.isEmpty() ? zoneID : null);
			jsonObject.put("message_id", messageID != null && !messageID.isEmpty() ? messageID : null);
			return jsonObject.toString().getBytes();
		} catch (Exception unused) {
			return null;
		}
	}
	
	/**
	 * Generates a random id for the message
	 */
	public void generateMessageID() {
		setMessageID(UUID.randomUUID().toString());
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		Message message1 = (Message) o;
		
		return timestamp == message1.timestamp &&
			origin.equals(message1.origin) &&
			destination.equals(message1.destination) &&
			message.equals(message1.message) &&
			clientID.equals(message1.clientID) &&
			(zoneID != null ? zoneID.equals(message1.zoneID) : message1.zoneID == null &&
				(messageID != null ? messageID.equals(message1.messageID) : message1.messageID == null));
		
	}
	
	@Override
	public int hashCode() {
		int result = origin.hashCode();
		result = 31 * result + destination.hashCode();
		result = 31 * result + message.hashCode();
		result = 31 * result + (int) (timestamp ^ (timestamp >>> 32));
		result = 31 * result + clientID.hashCode();
		result = 31 * result + (zoneID != null ? zoneID.hashCode() : 0);
		result = 31 * result + (messageID != null ? messageID.hashCode() : 0);
		return result;
	}
	
	@Override
	public String toString() {
		return "Message{" +
			"origin='" + origin + '\'' +
			", destination='" + destination + '\'' +
			", message='" + message + '\'' +
			", timestamp=" + timestamp +
			", clientID='" + clientID + '\'' +
			", zoneID='" + zoneID + '\'' +
			", messageID='" + messageID + '\'' +
			'}';
	}
}