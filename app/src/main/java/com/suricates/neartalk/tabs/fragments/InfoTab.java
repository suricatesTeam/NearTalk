package com.suricates.neartalk.tabs.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.suricates.neartalk.R;
import com.suricates.neartalk.utils.Const;
import com.suricates.neartalk.utils.Preferences;
import com.suricates.neartalk.utils.SLog;
import com.suricates.neartalk.utils.U;

/**
 * Fragment used to display information about the user, and its current zone
 */
public class InfoTab extends Fragment implements OnMapReadyCallback {
	
	private GoogleMap mGoogleMap;
	private MapFragment mMapFragment;
	private View rootView;
	
	private TextView logged;
	private TextView connections;
	
	private Context mContext;
	private Circle mZoneCircle;
	
	private BroadcastReceiver mBroadcastReceiver;
	
	public InfoTab() {
	}
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		mContext = context;
		
		startReceiver();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mContext = activity;
		
		startReceiver();
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		
		if (mContext != null) {
			Preferences.set(getActivity(), Const.sp.GLOBAL_CHAT_VISIBLE, !isVisibleToUser);
			updateMapCameraFromPreferences();
		}
	}
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		
		if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP)
			mMapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragmentZoneLocation);
		else
			mMapFragment = (MapFragment) getChildFragmentManager().findFragmentById(R.id.mapFragmentZoneLocation);
		mMapFragment.onCreate(savedInstanceState);
		mMapFragment.getMapAsync(this);
		
		logged = (TextView) view.findViewById(R.id.logged_as);
		logged.setText(
			mContext.getString(R.string.logged_as) + " " +
				Preferences.getString(mContext, Const.sp.USER_NAME, mContext.getString(R.string.anon))
		);
		
		final String[] users = Preferences.getString(mContext, Const.sp.CURR_ZONE_USERS, mContext.getString(R.string.not_available))
			.split(Const.sp.SEPARATOR);
		int numConnections = users.length;
		connections = (TextView) view.findViewById(R.id.connectedUsersTextView);
		connections.setText(mContext.getResources().getQuantityString(
			R.plurals.num_connected_user, numConnections, numConnections)
		);
		connections.setOnClickListener(v ->
			new AlertDialog.Builder(mContext)
				.setTitle(R.string.users_connected)
				.setAdapter(
					new ArrayAdapter<>(
						mContext,
						android.R.layout.simple_list_item_activated_1,
						users
					),
					null
				).setCancelable(true)
				.create().show()
		);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		if (mMapFragment != null) {
			mMapFragment.onPause();
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (mMapFragment != null) {
			mMapFragment.onResume();
		}
	}
	
	@Override
	public void onDestroyView() {
		super.onDestroyView();
		if (mMapFragment != null) {
			mMapFragment.onDestroyView();
		}
	}
	
	@Override
	public void onLowMemory() {
		super.onLowMemory();
		if (mMapFragment != null) {
			mMapFragment.onLowMemory();
		}
	}
	
	@Override
	public void onStop() {
		super.onStop();
		U.silentlyUnregisterReceiver(mContext, mBroadcastReceiver);
		mBroadcastReceiver = null;
	}
	
	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		MapsInitializer.initialize(getActivity());
		
		if (rootView != null) {
			ViewGroup parent = (ViewGroup) rootView.getParent();
			if (parent != null)
				parent.removeView(rootView);
		}
		try {
			rootView = inflater.inflate(R.layout.info_tab, container, false);
		} catch (Exception ignored) {
		}
		return rootView;
	}
	
	@SuppressWarnings("MissingPermission")
	@Override
	public void onMapReady(GoogleMap googleMap) {
		if (mGoogleMap == null) {
			mGoogleMap = googleMap;
			
			mGoogleMap.setOnMapClickListener(latLng -> {
			});
			mGoogleMap.setOnMapLongClickListener(latLng -> {
			});
			mGoogleMap.setMyLocationEnabled(true);
			mGoogleMap.getUiSettings().setMapToolbarEnabled(false);
			mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
			mGoogleMap.getUiSettings().setAllGesturesEnabled(false);
			
			updateMapCameraFromPreferences();
		}
	}
	
	/**
	 * Starts the receiver to update the view with new values
	 */
	private void startReceiver() {
		if (mBroadcastReceiver == null && mContext != null) {
			mContext.registerReceiver(mBroadcastReceiver = new BroadcastReceiver() {
				@Override
				public void onReceive(Context context, Intent intent) {
					if (intent == null) return;
					switch (intent.getAction()) {
						case Const.actions.UPDATE_SHOW_USERNAME:
							if (logged == null || mContext == null) return;
							
							logged.setText(
								mContext.getString(R.string.logged_as) + " " +
									Preferences.getString(mContext, Const.sp.USER_NAME, mContext.getString(R.string.anon))
							);
							break;
						case Const.actions.ROOM_UPDATED:
							if (connections == null || mContext == null) return;
							final String[] users = Preferences.getString(mContext, Const.sp.CURR_ZONE_USERS, mContext.getString(R.string.not_available))
								.split(Const.sp.SEPARATOR);
							int numConnections = users.length;
							connections.setText(
								mContext.getResources().getQuantityString(
									R.plurals.num_connected_user, numConnections, numConnections)
							);
							
							connections.setOnClickListener(v ->
								new AlertDialog.Builder(mContext)
									.setTitle(R.string.users_connected)
									.setAdapter(
										new ArrayAdapter<>(
											mContext,
											android.R.layout.simple_list_item_activated_1,
											users),
										null
									).setCancelable(true)
									.create().show()
							);
							updateMapCameraFromPreferences();
							break;
						case Const.actions.UPDATE_LOCATION:
							SLog.e("Location updated!");
							updateMapCameraFromPreferences();
							break;
						default:
							SLog.e("Don't know what to do with action: " + intent.getAction());
							break;
					}
				}
			}, new IntentFilter() {{
				addAction(Const.actions.ROOM_UPDATED);
				addAction(Const.actions.UPDATE_LOCATION);
			}});
		}
		LocalBroadcastManager.getInstance(mContext).registerReceiver(
			mBroadcastReceiver,
			new IntentFilter(Const.actions.UPDATE_SHOW_USERNAME)
		);
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mMapFragment.onConfigurationChanged(newConfig);
	}
	
	/**
	 * Updates where to display the circle showing your current zone
	 */
	private void updateMapCameraFromPreferences() {
		if (mGoogleMap == null) {
			SLog.e("Trying to update map camera when map is null");
			return;
		}
		
		float lat = Float.parseFloat(Preferences.getString(mContext, Const.sp.CURR_LAT, "-1.0"));
		float lon = Float.parseFloat(Preferences.getString(mContext, Const.sp.CURR_LON, "-1.0"));
		
		float zone_lat = Float.parseFloat(Preferences.getString(mContext, Const.sp.CURR_ZONE_LAT, "-1.0"));
		float zone_lon = Float.parseFloat(Preferences.getString(mContext, Const.sp.CURR_ZONE_LON, "-1.0"));
		
		if (lat != -1 && lon != -1) {
			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lon), 14));
			
			if (mZoneCircle != null) {
				mZoneCircle.setCenter(new LatLng(zone_lat, zone_lon));
			} else {
				mZoneCircle = mGoogleMap.addCircle(new CircleOptions()
					.center(new LatLng(zone_lat, zone_lon))
					.radius(500)
					.strokeColor(Color.BLACK)
					.fillColor(Color.argb(125, 219, 112, 147))
				);
			}
		} else {
			SLog.e("Trying to update map camera lat or lon are -1");
			mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(40.705311, -74.258188), 14));
		}
	}
}