package com.suricates.neartalk.tabs.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Handler;
import android.os.IBinder;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.Vibrator;
import android.support.annotation.StringRes;
import android.support.design.widget.FloatingActionButton;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.suricates.neartalk.R;
import com.suricates.neartalk.adapters.ChatMessageAdapter;
import com.suricates.neartalk.adapters.models.ChatMessage;
import com.suricates.neartalk.client.MQTTService;
import com.suricates.neartalk.client.models.Message;
import com.suricates.neartalk.utils.Const;
import com.suricates.neartalk.utils.Preferences;
import com.suricates.neartalk.utils.SLog;
import com.suricates.neartalk.utils.U;
import com.suricates.neartalk.utils.DBHelper;

import java.io.File;
import java.util.LinkedList;

/**
 * Fragment used to send and display the messages
 */
public class GlobalChat extends Fragment implements MQTTService.OnMessageFromServiceReceivedListener {
	
	private ChatMessageAdapter chatMessageAdapter;
	private FloatingActionButton scrollFabIcon;
	private int mWidth = -1;
	private int mHeight = -1;
	
	private Context mContext;
	private String mOrigin;
	private String mClientID;
	
	private EditText input;
	
	private View serviceError;
	private TextView serviceErrorMessage;
	
	private DBHelper dbHelper;
	
	private Messenger mClientToServiceMsg;
	private final Messenger serviceToClientMsg = new Messenger(new IncomingMsgHandler(this));
	
	/**
	 * The ServiceConnection receives the Messenger instance from the service binder that will allow
	 * the service to communicate with the GlobalChat fragment.
	 * <p>
	 * When the instance is received, it sends a message setting the replyTo attribute to our
	 * local Messenger that allows the GlobalChat fragment communicate with the service
	 */
	private ServiceConnection mServiceConnection = new ServiceConnection() {
		
		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			mClientToServiceMsg = new Messenger(service);
			android.os.Message binderMessage = android.os.Message.obtain();
			binderMessage.replyTo = serviceToClientMsg;
			binderMessage.what = Const.mqtt.SERVICE_BINDER_MESSAGE_WHAT;
			try {
				mClientToServiceMsg.send(binderMessage);
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			SLog.d("Service connected: " + service.getClass());
		}
		
		@Override
		public void onServiceDisconnected(ComponentName name) {
			SLog.d("Service disconnected");
			mClientToServiceMsg = null;
		}
	};
	
	private View mFragmentView;
	
	/**
	 * This Handler is used to parse incoming messages sent from the service to our fragment
	 * When this happens, it triggers the callback of the interface OnMessageFromServiceReceivedListener
	 */
	private static class IncomingMsgHandler extends Handler {
		
		private GlobalChat mGlobalChat;
		
		IncomingMsgHandler(GlobalChat globalChat) {
			mGlobalChat = globalChat;
		}
		
		@Override
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
				case Const.mqtt.SERVICE_INCOMING_MESSAGE_WHAT: {
					Bundle data = msg.getData();
					if (data != null) {
						Message outMessage = (Message) data.getSerializable(Const.mqtt.SERVICE_INCOMING_MESSAGE_BUNDLE_KEY);
						mGlobalChat.messageFromServiceReceived(outMessage);
					} else {
						SLog.e("Couldn't get the data from the outgoing message");
					}
				}
				break;
				case Const.mqtt.SERVICE_GLOBAL_CONNECTED_WHAT: {
					mGlobalChat.hideErrorMessage();
					mGlobalChat.input.setEnabled(true);
				}
				break;
				case Const.mqtt.SERVICE_NO_GPS_WHAT: {
					mGlobalChat.showErrorMessage(R.string.global_chat_no_gps_label);
					mGlobalChat.input.setEnabled(false);
				}
				break;
				case Const.mqtt.SERVICE_NO_NETWORK_WHAT: {
					mGlobalChat.showErrorMessage(R.string.global_chat_no_network_label);
					mGlobalChat.input.setEnabled(false);
				}
				break;
				case Const.mqtt.SERVICE_GLOBAL_STOPPED_WHAT: {
					mGlobalChat.showErrorMessage(R.string.global_chat_service_stopped_label);
					mGlobalChat.input.setEnabled(false);
				}
				break;
				case Const.mqtt.SERVICE_WAITING_GPS_WHAT: {
					mGlobalChat.showErrorMessage(R.string.waiting_for_gps);
					mGlobalChat.input.setEnabled(false);
				}
				break;
				default:
					super.handleMessage(msg);
			}
		}
	}
	
	private LinkedList<ChatMessage> mChatMessages = new LinkedList<>();
	
	public GlobalChat() {
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		startGlobalChatInstance(activity.getApplicationContext());
	}
	
	@Override
	public void onAttach(Context context) {
		super.onAttach(context);
		startGlobalChatInstance(context);
	}
	
	/**
	 * Starts the service to handle the messages, and displays the messages
	 *
	 * @param context the app context
	 */
	private void startGlobalChatInstance(Context context) {
		if (mContext == null) {
			mContext = context;
			
			mOrigin = Preferences.getString(
				mContext,
				Const.sp.USER_NAME,
				mContext.getString(R.string.anon) + System.currentTimeMillis() / 1000000
			);
			
			mClientID = Preferences.getString(
				mContext,
				Const.sp.USER_ID,
				null
			);
			
			mContext.startService(new Intent(mContext, MQTTService.class));
			
			LocalBroadcastManager.getInstance(mContext).registerReceiver(mBroadcastReceiver,
				new IntentFilter() {{
					addAction(Const.actions.UPDATE_BACKGROUND);
					addAction(Const.actions.UPDATE_USERNAME);
					addAction(Const.fab.SCROLL_MESSAGES_ACTION);
				}});
			
			mContext.registerReceiver(
				mBroadcastReceiver,
				new IntentFilter(Const.actions.ZONE_CHANGED)
			);
			
			DBHelper dbHelper = getDbHelper();
			try {
				//noinspection Convert2streamapi
				for (Message message : dbHelper.getMessagesFromZone(Const.messages.OFFSET)) {
					mChatMessages.add(new ChatMessage(
						message.getOrigin(),
						message.getMessage(),
						String.valueOf(message.getTimestamp()),
						message.getClientID().equalsIgnoreCase(mClientID),
						true,
						message.isSent(),
						message.getMessageID()));
				}
			} catch (SQLiteException sqlException) {
				if (sqlException.getMessage().contains("no such table: zone_messages")) {
					dbHelper.deleteZoneMessages();
					dbHelper.createZoneDatabase();
				}
			}
		} else {
			SLog.e("Trying to connect GlobalChat fragment, context is null!");
		}
	}
	
	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.global_chat, container, false);
	}
	
	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		mFragmentView = view;
		
		serviceError = view.findViewById(R.id.service_error_box);
		serviceErrorMessage = (TextView) view.findViewById(R.id.service_error_message);
		
		String res = Preferences.getString(mContext, Const.sp.CHAT_BG, null);
		
		ViewTreeObserver viewTreeObserver = view.getViewTreeObserver();
		if (viewTreeObserver.isAlive()) {
			viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
					mWidth = view.getWidth();
					mHeight = view.getHeight();
					
					setImageBackground(res);
				}
			});
		}
		
		scrollFabIcon = (FloatingActionButton) view.findViewById(R.id.scrollFab);
		scrollFabIcon.hide();
		
		scrollFabIcon.setOnClickListener(v ->
			LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent(Const.fab.CLICK_ACTION))
		);
		
		input = (EditText) view.findViewById(R.id.input_msg);
		//Send button
		view.findViewById(R.id.send_btn).setOnClickListener(v -> {
			String msg = input.getText().toString().trim();
			if (msg.isEmpty()) return;
			
			Message outMessage = new Message(
				mOrigin,
				"global",
				Preferences.getString(getActivity(), Const.sp.USER_ID, null),
				msg,
				System.currentTimeMillis()
			);
			outMessage.generateMessageID();
			if (mClientToServiceMsg != null) {
				try {
					android.os.Message sentMessage = android.os.Message.obtain();
					Bundle data = new Bundle();
					data.putSerializable(Const.mqtt.SERVICE_OUTGOING_MESSAGE_BUNDLE_KEY, outMessage);
					sentMessage.setData(data);
					sentMessage.what = Const.mqtt.SERVICE_OUTGOING_MESSAGE_WHAT;
					mClientToServiceMsg.send(sentMessage);
					
					chatMessageAdapter.add(new ChatMessage(
						outMessage.getOrigin(),
						outMessage.getMessage(),
						String.valueOf(outMessage.getTimestamp()),
						true,
						true,
						outMessage.isSent(),
						outMessage.getMessageID())
					);
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
			getDbHelper().addMessageToDatabase(outMessage);
			input.setText("");
		});
		
		RecyclerView messages = (RecyclerView) view.findViewById(R.id.list_messages);
		
		chatMessageAdapter = new ChatMessageAdapter(mContext, mChatMessages, messages, mClientID);
		messages.setAdapter(chatMessageAdapter);
		messages.setLayoutManager(chatMessageAdapter.getLayoutManager());
		
		input.setEnabled(false);
	}
	
	@Override
	public void onStart() {
		super.onStart();
		bindMQTTService();
	}
	
	/**
	 * Binds the MQTTService to send the messages
	 */
	private void bindMQTTService() {
		mContext.bindService(new Intent(mContext, MQTTService.class),
			mServiceConnection,
			Context.BIND_AUTO_CREATE
		);
	}
	
	@Override
	public void onStop() {
		super.onStop();
		
		try {
			mContext.unbindService(mServiceConnection);
		} catch (IllegalArgumentException ignored) { //Already unbinded
		}
		
		chatMessageAdapter.unregisterFabReceiver();
		U.silentlyUnregisterReceiver(mContext, mBroadcastReceiver);
	}
	
	@Override
	public void messageFromServiceReceived(Message message) {
		if (!message.getClientID().equalsIgnoreCase(mClientID)) {
			chatMessageAdapter.add(new ChatMessage(
				message.getOrigin(),
				message.getMessage(),
				String.valueOf(message.getTimestamp()),
				false,
				true,
				message.isSent(),
				message.getMessageID())
			);
		} else {
			chatMessageAdapter.setToSent(message.getMessageID());
			Vibrator v = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
			v.vibrate(10);
		}
	}
	
	/**
	 * Changes the current chat background image
	 *
	 * @param image the image to set
	 */
	private void setImageBackground(String image) {
		if (mFragmentView != null) {
			ImageView backgroundImageView = (ImageView) mFragmentView.findViewById(R.id.background_image_view);
			
			if (image != null) {
				String path = U.absolutePathFromUri(mContext, Uri.parse(image));
				if (path != null) {
					try {
						Bitmap bitmap = BitmapFactory.decodeFile(new File(path).getAbsolutePath(), new BitmapFactory.Options());
						backgroundImageView.setImageBitmap(scaleCropToFit(bitmap, mWidth, mHeight));
						return;
					} catch (Exception unused) {
						Preferences.remove(mContext, Const.sp.CHAT_BG);
					}
				}
			}
			
			Bitmap defaultImage = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.background_default);
			backgroundImageView.setImageBitmap(scaleCropToFit(defaultImage, mWidth, mHeight));
		} else {
			SLog.e("Trying to update background of global chat when view is null");
		}
	}
	
	/**
	 * Scales a Bitmap to the given width and height, so it fits in the screen
	 *
	 * @param original     the original image
	 * @param targetWidth  the target width
	 * @param targetHeight the target height
	 * @return a copy of the original Bitmap scaled
	 */
	public static Bitmap scaleCropToFit(Bitmap original, int targetWidth, int targetHeight) {
		//Need to scale the image, keeping the aspect ration first
		int width = original.getWidth();
		int height = original.getHeight();
		
		float widthScale = (float) targetWidth / (float) width;
		float heightScale = (float) targetHeight / (float) height;
		float scaledWidth;
		float scaledHeight;
		
		int startY = 0;
		int startX = 0;
		
		if (widthScale > heightScale) {
			scaledWidth = targetWidth;
			scaledHeight = height * widthScale;
			//crop height by...
			startY = (int) ((scaledHeight - targetHeight) / 2);
		} else {
			scaledHeight = targetHeight;
			scaledWidth = width * heightScale;
			//crop width by..
			startX = (int) ((scaledWidth - targetWidth) / 2);
		}
		
		Bitmap scaledBitmap = Bitmap.createScaledBitmap(original, (int) scaledWidth, (int) scaledHeight, true);
		
		return Bitmap.createBitmap(scaledBitmap, startX, startY, targetWidth, targetHeight);
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (getActivity() != null) {
			setVisible(isVisibleToUser);
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		setVisible(true);
		startGlobalChatInstance(mContext);
	}
	
	@Override
	public void onPause() {
		setVisible(false);
		super.onPause();
	}
	
	@Override
	public void onDestroy() {
		setVisible(false);
		super.onDestroy();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		
		ViewTreeObserver viewTreeObserver = mFragmentView.getViewTreeObserver();
		if (viewTreeObserver.isAlive()) {
			viewTreeObserver.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
				@Override
				public void onGlobalLayout() {
					mFragmentView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
					mWidth = mFragmentView.getWidth();
					mHeight = mFragmentView.getHeight();
					
					String res = Preferences.getString(mContext, Const.sp.CHAT_BG, null);
					setImageBackground(res);
				}
			});
		}
	}
	
	/**
	 * Sets if this fragment is visible at the moment or not
	 *
	 * @param isVisible true if it is visible, false otherwise
	 */
	private void setVisible(boolean isVisible) {
		Preferences.set(getActivity(), Const.sp.GLOBAL_CHAT_VISIBLE, isVisible);
		if (isVisible) {
			((NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE))
				.cancel(Const.notification.ID);
		} else {
			U.hideKeyboard(getActivity());
		}
		getDbHelper().deleteUnreadMessages();
	}
	
	/**
	 * Shows the error message label
	 *
	 * @param resId The message to show
	 */
	private void showErrorMessage(@StringRes int resId) {
		try {
			serviceError.setVisibility(View.VISIBLE);
			serviceErrorMessage.setText(mContext.getString(resId));
		} catch (Exception ignored) {
		}
	}
	
	/**
	 * Hides the error message label
	 */
	private void hideErrorMessage() {
		try {
			serviceError.setVisibility(View.GONE);
		} catch (Exception ignored) {
		}
	}
	
	/**
	 * BroadcastReceiver for all the events
	 */
	private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			switch (intent.getAction()) {
				case Const.fab.SCROLL_MESSAGES_ACTION:
					boolean show = intent.getBooleanExtra(Const.fab.SCROLL_MESSAGES_KEY, false);
					if (show) scrollFabIcon.show();
					else scrollFabIcon.hide();
					break;
				case Const.actions.UPDATE_BACKGROUND:
					String res = Preferences.getString(mContext, Const.sp.CHAT_BG, null);
					setImageBackground(res);
					break;
				case Const.actions.UPDATE_USERNAME:
					mOrigin = Preferences.getString(
						mContext,
						Const.sp.USER_NAME,
						mContext.getString(R.string.anon) + System.currentTimeMillis() / 1000000
					);
					break;
				case Const.actions.ZONE_CHANGED:
					if (mClientToServiceMsg != null) {
						android.os.Message sentMessage = android.os.Message.obtain();
						sentMessage.what = Const.mqtt.SERVICE_CHANGE_ZONE_WHAT;
						try {
							mClientToServiceMsg.send(sentMessage);
						} catch (RemoteException e) {
							e.printStackTrace();
						}
					}
					break;
				case Const.actions.REBIND_SERVICE:
					bindMQTTService();
					break;
				default:
					SLog.e("Don't know what to do with action: " + intent.getAction());
					break;
			}
			
		}
	};
	
	/**
	 * Gets a 'singleton' instance for the DBHelper on this fragment
	 *
	 * @return an instance
	 */
	private DBHelper getDbHelper() {
		if (this.dbHelper == null) {
			this.dbHelper = new DBHelper(getActivity());
		}
		return dbHelper;
	}
}
