package com.suricates.neartalk.tabs.fragments;

import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.content.LocalBroadcastManager;

import com.suricates.neartalk.R;
import com.suricates.neartalk.http.AsyncTaskManager;
import com.suricates.neartalk.utils.Const;
import com.suricates.neartalk.utils.Preferences;
import com.suricates.neartalk.utils.SLog;
import com.suricates.neartalk.utils.U;

/**
 * Fragment used to change the preferences of the app, along with some ui and user settings
 */
public class PreferencesFragment extends PreferenceFragment implements AsyncTaskManager.OnTaskCompletedListener {
	
	private EditTextPreference change_username;
	private String newUsername;
	
	private int task;
	private static final int TASK_UPDATE_USERNAME = 1;
	private static final int TASK_DELETE = 2;
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		getActivity().setTheme(R.style.PreferencesTheme);
		
		addPreferencesFromResource(R.xml.settings_preferences);
		
		Preference notification_sound = findPreference(getActivity().getString(R.string.choose_notification_sound_key));
		notification_sound.setOnPreferenceClickListener(preference -> {
			Intent intent = new Intent(RingtoneManager.ACTION_RINGTONE_PICKER);
			intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TITLE, getActivity().getString(R.string.select_notification_sound_intent));
			intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, false);
			
			//Check if there was a ringtone previously selected
			String ringtoneSaved = Preferences.getString(getActivity(), Const.sp.RINGTONE, null);
			if (ringtoneSaved != null) {
				intent.putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, Uri.parse(ringtoneSaved));
			}
			
			intent.putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true);
			intent.putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_RINGTONE);
			getActivity().startActivityForResult(intent, Const.codes.RINGTONE);
			return true;
		});
		
		Preference chat_bg = findPreference(getActivity().getString(R.string.change_chat_bg_key));
		chat_bg.setOnPreferenceClickListener(preference -> {
			U.intentPickPhoto(getActivity(), Const.codes.CHAT_BG);
			return true;
		});
		
		Preference reset_chat_bg = findPreference(getActivity().getString(R.string.reset_chat_bg_key));
		reset_chat_bg.setOnPreferenceClickListener(preference -> {
			U.confirmDialog(getActivity(), (dialog, which) -> {
				U.sendToast(R.string.reset_chat_bg_toast, getActivity());
				Preferences.remove(getActivity(), Const.sp.CHAT_BG);
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(Const.actions.UPDATE_BACKGROUND));
			});
			return true;
		});
		
		SwitchPreference global_bg_listener = (SwitchPreference) findPreference(getActivity().getString(R.string.global_chat_listener_key));
		global_bg_listener.setChecked(Preferences.getBoolean(getActivity(), getActivity().getString(R.string.global_chat_listener_key), true));
		global_bg_listener.setOnPreferenceChangeListener((preference, newValue) -> {
			Preferences.set(getActivity(), getActivity().getString(R.string.global_chat_listener_key), newValue);
			return true;
		});
		
		SwitchPreference switch_notification_sound = (SwitchPreference) findPreference(getActivity().getString(R.string.switch_notification_sound_key));
		switch_notification_sound.setChecked(Preferences.getBoolean(getActivity(), getActivity().getString(R.string.switch_notification_sound_key), true));
		switch_notification_sound.setOnPreferenceChangeListener((preference, newValue) -> {
			Preferences.set(getActivity(), getActivity().getString(R.string.switch_notification_sound_key), newValue);
			return true;
		});
		
		change_username = (EditTextPreference) findPreference(getActivity().getString(R.string.change_username_key));
		change_username.setDialogTitle(
			getActivity().getString(R.string.previous) + " " + Preferences.getString(getActivity(), Const.sp.USER_NAME, "...")
		);
		change_username.setText("");
		change_username.setOnPreferenceChangeListener((preference, newValue) -> {
			newUsername = ((String) newValue).trim();
			String prevUsername = Preferences.getString(getActivity(), Const.sp.USER_NAME, null);
			if (prevUsername != null && !prevUsername.equalsIgnoreCase(newUsername)) {
				task = TASK_UPDATE_USERNAME;
				new AsyncTaskManager(this).updateName(newUsername);
			}
			return true;
		});
		
		Preference logout = findPreference(getActivity().getString(R.string.logout_key));
		logout.setOnPreferenceClickListener(preference -> {
			U.confirmDialog(getActivity(), (dialog, which) -> U.logout(getActivity()));
			return true;
		});
		
		Preference delete = findPreference(getActivity().getString(R.string.delete_acc_key));
		delete.setOnPreferenceClickListener(preference -> {
			U.warningDialog(getActivity(), R.string.delete_acc_message, (dialog, which) -> {
				task = TASK_DELETE;
				U.sendToast(R.string.deleting_acc, getActivity());
				new AsyncTaskManager(this).deleteUser();
			});
			return true;
		});
	}
	
	/**
	 * @param error the error or -1 if there is no error
	 */
	@Override
	public void onTaskCompleted(@StringRes int error) {
		if (error != -1) {
			U.showErrorDialog(getActivity(), error);
			change_username.setText(newUsername);
			return;
		}
		
		switch (task) {
			case TASK_UPDATE_USERNAME:
				task = -1;
				change_username.setDialogTitle(
					getActivity().getString(R.string.previous) + " " + Preferences.getString(getActivity(), Const.sp.USER_NAME, "...")
				);
				change_username.setText("");
				newUsername = "";
				LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(Const.actions.UPDATE_USERNAME));
				U.sendToast(R.string.username_updated, getActivity());
				break;
			case TASK_DELETE:
				task = -1;
				U.logout(getActivity());
				U.sendToast(R.string.acc_deleted, getActivity());
				break;
			default:
				SLog.e("Should ot arrive here for this action (AsyncTaskManager): " + task);
				break;
		}
	}
	
	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (getActivity() != null) {
			Preferences.set(getActivity(), Const.sp.GLOBAL_CHAT_VISIBLE, !isVisibleToUser);
		}
	}
}