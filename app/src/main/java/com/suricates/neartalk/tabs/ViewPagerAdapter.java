package com.suricates.neartalk.tabs;

import android.app.Fragment;
import android.app.FragmentManager;
import android.support.annotation.Nullable;
import android.support.v13.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * A PagerAdapter to display the tabs with a ViewPager inside it
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
	private List<Fragment> mFragments = new ArrayList<>();
	private List<String> mTitles = new ArrayList<>();
	
	/**
	 * Creates the PagerAdapter
	 *
	 * @param fm the fragment manager to use
	 */
	public ViewPagerAdapter(FragmentManager fm) {
		super(fm);
	}
	
	/**
	 * Adds a new Tab, which is a fragment.
	 *
	 * @param fragment The fragment
	 * @param title    The name of the tab, null if the tab doesn't have a name
	 */
	public void addFragment(Fragment fragment, @Nullable String title) {
		mFragments.add(fragment);
		mTitles.add(title);
	}
	
	@Override
	public Fragment getItem(int position) {
		return mFragments.get(position);
	}
	
	@Override
	public CharSequence getPageTitle(int position) {
		return mTitles.get(position);
	}
	
	@Override
	public int getCount() {
		return mFragments.size();
	}
}