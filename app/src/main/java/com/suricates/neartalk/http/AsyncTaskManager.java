package com.suricates.neartalk.http;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.annotation.StringRes;
import android.support.v4.content.LocalBroadcastManager;

import com.suricates.neartalk.R;
import com.suricates.neartalk.utils.Const;
import com.suricates.neartalk.utils.Preferences;
import com.suricates.neartalk.utils.SLog;
import com.suricates.neartalk.utils.U;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * A helper class to make requests to the app's API
 */
public class AsyncTaskManager {
	
	/**
	 * Tasks codes
	 */
	private static final int UPDATE_LOCATION = 0;
	private static final int LOGIN = 1;
	private static final int REGISTER = 2;
	private static final int UPDATE_NAME = 3;
	private static final int DISCONNECT = 4;
	private static final int DELETE = 5;
	
	/**
	 * The API's base URL
	 */
	private static final String BASE_URL = "https://neartalk-restfulapi.herokuapp.com";
	
	private OnTaskCompletedListener mListener;
	
	private final Context mContext;
	private int task = -1;
	
	/**
	 * Creates the AsyncTaskManager, and sets a listener if the context implements the interface
	 * Used to create it from an activity or service
	 *
	 * @param context the app context
	 */
	public AsyncTaskManager(Context context) {
		this.mContext = context;
		if (this.mContext instanceof OnTaskCompletedListener) {
			mListener = (OnTaskCompletedListener) this.mContext;
		}
	}
	
	/**
	 * Creates the AsyncTaskManager, and sets a listener if the fragment implements the interface
	 * Used to create it from a fragment which implements the interface
	 *
	 * @param fragment the fragment from where it is created
	 */
	public AsyncTaskManager(Fragment fragment) {
		this.mContext = fragment.getActivity();
		if (fragment instanceof OnTaskCompletedListener) {
			mListener = (OnTaskCompletedListener) fragment;
		}
	}
	
	/**
	 * Updates the user's current position in the api
	 *
	 * @param lat the current latitude
	 * @param lon the current longitude
	 */
	public boolean updatePosition(Double lat, Double lon) {
		if (lat == null || lon == null) {
			SLog.e("Can't update position, latitude and longitude can't be null values");
			return false;
		}
		
		if (U.hasInternet(mContext)) {
			task = UPDATE_LOCATION;
			new APICALL().execute(lat, lon);
			return true;
		}
		return false;
	}
	
	/**
	 * Attempts to log in into the API
	 *
	 * @param email the user's email
	 */
	public boolean login(String email) {
		if (!U.isValidEmail(email)) {
			((Activity) mContext).runOnUiThread(() ->
				U.showErrorDialog(mContext, R.string.email_invalid)
			);
			return false;
		}
		
		if (!U.hasInternet(mContext)) {
			if (mListener != null) {
				mListener.onTaskCompleted(R.string.no_internet);
			}
			return false;
		}
		task = LOGIN;
		new APICALL().execute(email);
		return true;
	}
	
	/**
	 * Attempts to register against the API
	 *
	 * @param email the users email
	 * @param name  the users chosen nickname
	 */
	public boolean register(String email, String name) {
		if (!U.isValidEmail(email)) {
			((Activity) mContext).runOnUiThread(() ->
				U.showErrorDialog(mContext, R.string.email_invalid)
			);
			return false;
		}
		if (!U.isValidName(name)) {
			((Activity) mContext).runOnUiThread(() ->
				U.showErrorDialog(mContext, R.string.name_invalid)
			);
			return false;
		}
		
		if (!U.hasInternet(mContext)) {
			if (mListener != null) {
				mListener.onTaskCompleted(R.string.no_internet);
			}
			return false;
		}
		task = REGISTER;
		new APICALL().execute(name, email);
		return true;
	}
	
	/**
	 * Updates the users's nickname
	 *
	 * @param name the new nickname
	 */
	public boolean updateName(String name) {
		if (!U.isValidName(name)) {
			((Activity) mContext).runOnUiThread(() ->
				U.showErrorDialog(mContext, R.string.name_invalid)
			);
			return false;
		}
		
		if (!U.hasInternet(mContext)) {
			if (mListener != null) {
				mListener.onTaskCompleted(R.string.no_internet);
			}
			return false;
		}
		task = UPDATE_NAME;
		new APICALL().execute(name);
		return true;
	}
	
	/**
	 * Disconnects the user from the zone
	 */
	public boolean disconnectUser() {
		if (!U.hasInternet(mContext)) {
			if (mListener != null) {
				mListener.onTaskCompleted(R.string.no_internet);
			}
			return false;
		}
		task = DISCONNECT;
		new APICALL().execute();
		return true;
	}
	
	/**
	 * Deletes the user's account
	 */
	public boolean deleteUser() {
		if (!U.hasInternet(mContext)) {
			if (mListener != null) {
				mListener.onTaskCompleted(R.string.no_internet);
			}
			return false;
		}
		task = DELETE;
		new APICALL().execute();
		return true;
	}
	
	/**
	 * Helper class to make the requests to the api
	 */
	private class APICALL extends AsyncTask<Object, Integer, JSONObject> {
		
		private int error = -1;
		
		@Override
		protected JSONObject doInBackground(Object[] params) {
			if (task == -1) return null;
			
			final String id = Preferences.getString(mContext, Const.sp.USER_ID, null);
			
			try {
				HttpURLConnection conn;
				String line;
				StringBuilder json = new StringBuilder();
				BufferedReader reader;
				OutputStreamWriter out;
				switch (task) {
					case UPDATE_LOCATION:
						if (id == null) return null;
						try {
							conn = (HttpURLConnection) new URL(updateLocationUrl(id)).openConnection();
							conn.setDoOutput(true);
							conn.setRequestMethod("PUT");
							out = new OutputStreamWriter(conn.getOutputStream());
							
							JSONObject location = new JSONObject();
							location.put(Const.json.POSITION, new JSONArray() {{
								//mongo wants the coords inverted, [long, lat]
								put(params[1]);
								put(params[0]);
							}});
							out.write(location.toString());
							
							out.flush();
							out.close();
							conn.getOutputStream().close();
							conn.connect();
							
							reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
							error = conn.getResponseCode();
							if (error != HttpURLConnection.HTTP_OK) {
								reader.close();
								conn.getInputStream().close();
								conn.disconnect();
								return null;
							}
							
							while ((line = reader.readLine()) != null) {
								json.append(line);
							}
							
							reader.close();
							conn.getInputStream().close();
							conn.disconnect();
							
							Preferences.set(mContext, Const.sp.CURR_LAT, params[0].toString());
							Preferences.set(mContext, Const.sp.CURR_LON, params[1].toString());
							mContext.sendBroadcast(new Intent(Const.actions.UPDATE_LOCATION));
							
							return new JSONObject(json.toString());
						} catch (Exception e) {
							SLog.e("Could not update location: " + e.getMessage());
							((Activity) mContext).runOnUiThread(() ->
								new APICALL().execute(params[0], params[1])
							);
							return null;
						}
					case LOGIN:
						conn = (HttpURLConnection) new URL(loginUrl((String) params[0])).openConnection();
						conn.connect();
						
						error = conn.getResponseCode();
						if (error != HttpURLConnection.HTTP_OK) {
							conn.disconnect();
							return null;
						}
						
						reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						while ((line = reader.readLine()) != null) {
							json.append(line);
						}
						
						reader.close();
						conn.getInputStream().close();
						conn.disconnect();
						
						return new JSONObject(json.toString());
					case REGISTER: {
						conn = (HttpURLConnection) new URL(registerUrl()).openConnection();
						conn.setDoOutput(true);
						conn.setRequestMethod("POST");
						out = new OutputStreamWriter(conn.getOutputStream());
						
						JSONObject user = new JSONObject();
						user.put(Const.json.NAME, params[0]);
						user.put(Const.json.EMAIL, params[1]);
						out.write(user.toString());
						
						out.flush();
						out.close();
						conn.getOutputStream().close();
						conn.connect();
						
						error = conn.getResponseCode();
						boolean closeError = false;
						if (error >= HttpURLConnection.HTTP_NOT_FOUND) {
							reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
							closeError = true;
						} else {
							reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
						}
						while ((line = reader.readLine()) != null) {
							json.append(line);
						}
						
						reader.close();
						if (closeError) {
							conn.getErrorStream().close();
						} else {
							conn.getInputStream().close();
						}
						conn.disconnect();
						
						return new JSONObject(json.toString());
					}
					case UPDATE_NAME: {
						if (id == null) return null;
						conn = (HttpURLConnection) new URL(updateNameUrl(id)).openConnection();
						conn.setDoOutput(true);
						conn.setRequestMethod("PUT");
						out = new OutputStreamWriter(conn.getOutputStream());
						
						JSONObject name = new JSONObject();
						name.put(Const.json.NAME, params[0]);
						out.write(name.toString());
						
						out.flush();
						out.close();
						conn.getOutputStream().close();
						conn.connect();
						
						error = conn.getResponseCode();
						if (error != HttpURLConnection.HTTP_OK) {
							boolean closeError = false;
							if (error >= HttpURLConnection.HTTP_NOT_FOUND) {
								reader = new BufferedReader(new InputStreamReader(conn.getErrorStream()));
								closeError = true;
							} else {
								reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
							}
							while ((line = reader.readLine()) != null) {
								json.append(line);
							}
							
							reader.close();
							if (closeError) {
								conn.getErrorStream().close();
							} else {
								conn.getInputStream().close();
							}
							conn.disconnect();
							
							return new JSONObject(json.toString());
						}
						
						conn.disconnect();
						
						return new JSONObject() {{
							put(Const.json.NAME, params[0]);
						}};
					}
					case DISCONNECT:
						conn = (HttpURLConnection) new URL(disconnect(id)).openConnection();
						conn.setRequestMethod("PUT");
						conn.connect();
						error = conn.getResponseCode();
						conn.disconnect();
						return new JSONObject();
					case DELETE:
						conn = (HttpURLConnection) new URL(delete(id)).openConnection();
						conn.setRequestMethod("DELETE");
						conn.connect();
						error = conn.getResponseCode();
						conn.disconnect();
						return new JSONObject();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(JSONObject json) {
			super.onPostExecute(json);
			try {
				if (json == null || error != HttpURLConnection.HTTP_OK) {
					if (mListener != null) {
						switch (error) {
							case HttpURLConnection.HTTP_NOT_FOUND:
								if (task == LOGIN) {
									mListener.onTaskCompleted(R.string.user_not_found);
								}
								break;
							case HttpURLConnection.HTTP_CONFLICT:
								if (task == REGISTER) {
									@StringRes int err = -1;
									if (json != null) {
										switch (json.getString(Const.json.ERR)) {
											case Const.errors.EMAIL_TAKEN:
												err = R.string.email_taken;
												break;
											case Const.errors.NAME_TAKEN:
												err = R.string.name_taken;
												break;
											default:
												SLog.e("Unknown error code: " + json.getString(Const.json.ERR));
												break;
										}
									}
									mListener.onTaskCompleted(
										err != -1 ? err : R.string.already_exists
									);
								} else if (task == UPDATE_NAME) {
									@StringRes int err = -1;
									if (json != null &&
										json.getString(Const.json.ERR).equalsIgnoreCase(Const.errors.NAME_TAKEN)) {
										err = R.string.name_taken;
									}
									mListener.onTaskCompleted(err);
								}
								break;
						}
					}
					return;
				}
				
				switch (task) {
					case UPDATE_LOCATION:
						final String newZone = json.getString(Const.json.ROOM_ID);
						final String currZone = Preferences.getString(mContext, Const.sp.CURRENT_ZONE, "");
						if (currZone.isEmpty() || !currZone.equalsIgnoreCase(newZone)) {
							if (!currZone.isEmpty()) {
								mContext.sendBroadcast(new Intent(Const.actions.ZONE_CHANGED));
							}
							
							Preferences.set(
								mContext,
								Const.sp.CURRENT_ZONE,
								newZone
							);
						}
						
						JSONArray coordinates = json.getJSONObject(Const.json.LOCATION).getJSONArray(Const.json.COORDINATES);
						Preferences.set(
							mContext,
							Const.sp.CURR_ZONE_LAT,
							String.valueOf(coordinates.getDouble(1))
						);
						Preferences.set(
							mContext,
							Const.sp.CURR_ZONE_LON,
							String.valueOf(coordinates.getDouble(0))
						);
						
						JSONArray array = json.getJSONArray(Const.json.ROOM_USERS);
						StringBuilder users = new StringBuilder();
						for (int i = 0; i < array.length(); i++) {
							users.append(array.getString(i));
							if (i != array.length() - 1) {
								users.append(Const.sp.SEPARATOR);
							}
						}
						
						String currentUsers = Preferences.getString(mContext, Const.sp.CURR_ZONE_USERS, "");
						if (currentUsers.isEmpty() || !currentUsers.equalsIgnoreCase(users.toString())) {
							Preferences.set(
								mContext,
								Const.sp.CURR_ZONE_USERS,
								users.toString()
							);
							
							mContext.sendBroadcast(new Intent(Const.actions.ROOM_UPDATED));
						}
						break;
					case REGISTER:
					case LOGIN:
						Preferences.set(
							mContext,
							Const.sp.USER_ID,
							json.getString(Const.json.USER_ID)
						);
						Preferences.set(
							mContext,
							Const.sp.USER_NAME,
							json.getString(Const.json.NAME)
						);
						break;
					case UPDATE_NAME:
						Preferences.set(
							mContext,
							Const.sp.USER_NAME,
							json.getString(Const.json.NAME)
						);
						LocalBroadcastManager.getInstance(mContext).sendBroadcast(new Intent(Const.actions.UPDATE_SHOW_USERNAME));
						break;
					case DISCONNECT:
					case DELETE:
						break;
					default:
						SLog.e("AsyncTaskManager.onPostExecute() | Should not get here");
						break;
				}
				if (mListener != null) {
					mListener.onTaskCompleted(-1);
				}
			} catch (JSONException ignored) {
			}
		}
	}
	
	/**
	 * Gets to Url where to update the location
	 *
	 * @param id the user's id
	 * @return the Url
	 */
	private String updateLocationUrl(String id) {
		return BASE_URL + "/users/" + id + "/coords";
	}
	
	/**
	 * Gets the Url where to attempt the login
	 *
	 * @param email the user's email
	 * @return the Url
	 */
	private String loginUrl(String email) {
		return BASE_URL + "/login?email=" + email;
	}
	
	/**
	 * Gets the Url where to register
	 *
	 * @return the Url
	 */
	private String registerUrl() {
		return BASE_URL + "/users";
	}
	
	/**
	 * Gets the Url where to update the username
	 *
	 * @param id the user's id
	 * @return the Url
	 */
	private String updateNameUrl(String id) {
		return BASE_URL + "/users/" + id + "/name";
	}
	
	/**
	 * Gets the Url where to disconnect the user
	 *
	 * @param id the user's id
	 * @return the Url
	 */
	private String disconnect(String id) {
		return BASE_URL + "/users/" + id + "/disconnect";
	}
	
	/**
	 * Gets the Url where to delete the user
	 *
	 * @param id the user's id
	 * @return the Url
	 */
	private String delete(String id) {
		return BASE_URL + "/users/" + id;
	}
	
	/**
	 * Interface to know when the task has been completed
	 */
	public interface OnTaskCompletedListener {
		/**
		 * The current task has been completed
		 *
		 * @param error the error or -1 if there is no error
		 */
		void onTaskCompleted(@StringRes int error);
	}
}
