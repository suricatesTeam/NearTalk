package com.suricates.neartalk.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.suricates.neartalk.R;
import com.suricates.neartalk.client.models.Message;

import java.util.Collections;
import java.util.LinkedList;


/**
 * A helper class to get information stored in databases
 */
public class DBHelper extends SQLiteOpenHelper {
	
	private static final String DB_MESSAGES_NAME = "messages_db.db";
	
	private static final String ZONE_MESSAGES_TABLE = "zone_messages";
	private static final String UNREAD_MESSAGES_TABLE = "unread_messages";
	
	private static final String COLUMN_MESSAGE_ID = "messageid";
	private static final String COLUMN_ORIGIN = "origin";
	private static final String COLUMN_DESTINATION = "destination";
	private static final String COLUMN_ZONE = "zoneid";
	private static final String COLUMN_CLIENT = "clientid";
	private static final String COLUMN_MESSAGE = "message";
	private static final String COLUMN_TIMESTAMP = "timestamp";
	private static final String COLUMN_SENT = "sent";
	
	private static final String TRIGGER_NAME = "autoinsert_unread_messages";
	
	private Context mContext;
	
	/**
	 * Creates the helper for the messages database
	 *
	 * @param context the app context
	 */
	public DBHelper(Context context) {
		super(context, DB_MESSAGES_NAME, null, 1);
		this.mContext = context;
	}
	
	@Override
	public void onCreate(SQLiteDatabase db) {
	}
	
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}
	
	/**
	 * Creates the tables and triggers for the messages database
	 */
	public void createZoneDatabase() {
		SLog.i("Creating zone messages database for: " + ZONE_MESSAGES_TABLE);
		
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("CREATE TABLE IF NOT EXISTS " + ZONE_MESSAGES_TABLE +
			"(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
			COLUMN_MESSAGE_ID + " TEXT UNIQUE, " +
			COLUMN_ORIGIN + " TEXT, " +
			COLUMN_DESTINATION + " TEXT, " +
			COLUMN_ZONE + " TEXT, " +
			COLUMN_CLIENT + " TEXT, " +
			COLUMN_MESSAGE + " TEXT, " +
			COLUMN_SENT + " INT, " +
			COLUMN_TIMESTAMP + " LONG);");
		
		db.execSQL("CREATE TABLE IF NOT EXISTS " + UNREAD_MESSAGES_TABLE +
			"(" + BaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
			COLUMN_MESSAGE_ID + " TEXT UNIQUE, " +
			COLUMN_MESSAGE + " TEXT, " +
			COLUMN_ORIGIN + " TEXT);");
		
		db.execSQL("CREATE TRIGGER IF NOT EXISTS " + TRIGGER_NAME +
			" AFTER INSERT ON " + ZONE_MESSAGES_TABLE + " WHEN NEW." + COLUMN_CLIENT + " NOT LIKE '"
			+ Preferences.getString(mContext, Const.sp.USER_ID, "") +
			"' BEGIN" +
			" INSERT INTO " + UNREAD_MESSAGES_TABLE + "(" + COLUMN_MESSAGE_ID + ", " + COLUMN_ORIGIN + ", "
			+ COLUMN_MESSAGE + ") VALUES (NEW." + COLUMN_MESSAGE_ID + ", NEW." + COLUMN_ORIGIN + ", NEW." + COLUMN_MESSAGE + ");" +
			" END;");
		db.close();
	}
	
	/**
	 * Gets the number of unread messages, for the notification
	 *
	 * @param inboxStyle the notifications Notification.InboxStyle
	 * @return the number of unread messages
	 */
	int getUnreadMessages(NotificationCompat.InboxStyle inboxStyle) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor messages = db.query(UNREAD_MESSAGES_TABLE, new String[]{COLUMN_ORIGIN, COLUMN_MESSAGE}, null
			, null, null, null, BaseColumns._ID + " DESC", String.valueOf(Const.notification.LIMIT_MESSAGES));
		while (messages.moveToNext()) {
			inboxStyle.addLine(messages.getString(messages.getColumnIndex(COLUMN_ORIGIN)) + ": "
				+ messages.getString(messages.getColumnIndex(COLUMN_MESSAGE)));
		}
		messages.close();
		int numOfMessages = countUnreadMessages(db);
		if (numOfMessages > Const.notification.LIMIT_MESSAGES) {
            int num = (numOfMessages - Const.notification.LIMIT_MESSAGES);
            inboxStyle.addLine(mContext.getString(R.string.and) + " "
                    + mContext.getResources().getQuantityString(R.plurals.messages_more,num, num));
		}
		db.close();
		return numOfMessages;
	}
	
	/**
	 * Counts the how many unread messages has the user
	 *
	 * @param db the database
	 * @return the number of unread messages
	 */
	private int countUnreadMessages(@Nullable SQLiteDatabase db) {
		if (db == null)
			db = this.getReadableDatabase();
		
		Cursor count = db.rawQuery("SELECT COUNT(*) FROM " + UNREAD_MESSAGES_TABLE, null);
		count.moveToNext();
		int numOfMessages = count.getInt(0);
		count.close();
		return numOfMessages;
	}
	
	/**
	 * Drop trigger when user logout.
	 */
	void dropUnReadMessagesTrigger() {
		SQLiteDatabase db = this.getReadableDatabase();
		db.execSQL("DROP TRIGGER " + TRIGGER_NAME);
	}
	
	/**
	 * Deletes the unread messages
	 *
	 * @return the number of deleted messages.
	 */
	public int deleteUnreadMessages() {
		SQLiteDatabase db = this.getReadableDatabase();
		int num = 0;
		try {
			num = db.delete(UNREAD_MESSAGES_TABLE, null, null);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		db.close();
		return num;
	}
	
	/**
	 * Adds a new message to the database
	 *
	 * @param message the message to store
	 * @return the result of the insert
	 */
	public long addMessageToDatabase(Message message) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues content = new ContentValues();
		
		content.put(COLUMN_MESSAGE_ID, message.getMessageID());
		content.put(COLUMN_ORIGIN, message.getOrigin());
		content.put(COLUMN_DESTINATION, message.getDestination());
		content.put(COLUMN_ZONE, message.getZoneID());
		content.put(COLUMN_CLIENT, message.getClientID());
		content.put(COLUMN_MESSAGE, message.getMessage());
		content.put(COLUMN_TIMESTAMP, message.getTimestamp());
		content.put(COLUMN_SENT, message.isSent());
		
		SLog.d("Inserting message to database: " + message);
		long result = -1;
		try {
			result = db.insert(ZONE_MESSAGES_TABLE, null, content);
		} catch (SQLiteConstraintException e) {
			SLog.e("Error inserting message to database: " + message);
			SLog.e("\tError -> " + e.getMessage());
		}
		db.close();
		return result;
	}
	
	/**
	 * Sets the message status as sent
	 *
	 * @param message the message which to change its status
	 */
	public void setMessageSent(Message message) {
		SQLiteDatabase db = this.getWritableDatabase();
		
		ContentValues content = new ContentValues();
		content.put(COLUMN_SENT, 1);
		
		db.update(ZONE_MESSAGES_TABLE, content, COLUMN_MESSAGE_ID + " = ?", new String[]{message.getMessageID()});
	}
	
	/**
	 * Gets all the messages which are not send
	 *
	 * @return a list with all the messages
	 */
	public LinkedList<Message> getNotSentMessages() {
		SQLiteDatabase db = this.getReadableDatabase();
		LinkedList<Message> messagesList = new LinkedList<>();
		
		Cursor res = db.query(ZONE_MESSAGES_TABLE, null, COLUMN_SENT + " = 0", null, null, null, COLUMN_TIMESTAMP);
		
		res.moveToFirst();
		while (!res.isAfterLast()) {
			messagesList.add(new Message(
				res.getString(res.getColumnIndex(COLUMN_ORIGIN)),
				res.getString(res.getColumnIndex(COLUMN_DESTINATION)),
				res.getString(res.getColumnIndex(COLUMN_MESSAGE)),
				res.getString(res.getColumnIndex(COLUMN_MESSAGE_ID)),
				res.getLong(res.getColumnIndex(COLUMN_TIMESTAMP)),
				res.getString(res.getColumnIndex(COLUMN_CLIENT)),
				res.getString(res.getColumnIndex(COLUMN_ZONE)),
				false));
			
			res.moveToNext();
		}
		res.close();
		db.close();
		
		return messagesList;
	}
	
	/**
	 * Gets the current zone messages
	 *
	 * @param limit the limit of messages to get
	 * @return a list with all the messages
	 */
	public LinkedList<Message> getMessagesFromZone(int limit) {
		return getMessagesFromZone(limit, -1, true);
	}
	
	/**
	 * Gets the current zone messages
	 *
	 * @param limit           the limit of messages to get
	 * @param timestampOffset an offset of time
	 * @param reverse         true in reverse order, false otherwise
	 * @return a list with all the messages
	 */
	public LinkedList<Message> getMessagesFromZone(int limit, long timestampOffset, boolean reverse) {
		LinkedList<Message> messagesList = new LinkedList<>();
		
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor res;
		
		if (limit == -1) {
			res = db.query(ZONE_MESSAGES_TABLE, null, null, null, null, null, COLUMN_TIMESTAMP + " DESC", null);
		} else if (timestampOffset == -1) {
			res = db.query(ZONE_MESSAGES_TABLE, null, null, null, null, null, COLUMN_TIMESTAMP + " DESC", String.valueOf(limit));
		} else {
			res = db.query(ZONE_MESSAGES_TABLE, null, COLUMN_TIMESTAMP + " < ?", new String[]{String.valueOf(timestampOffset)}, null, null, COLUMN_TIMESTAMP + " DESC", String.valueOf(limit));
		}
		
		res.moveToFirst();
		while (!res.isAfterLast()) {
			messagesList.add(new Message(
				res.getString(res.getColumnIndex(COLUMN_ORIGIN)),
				res.getString(res.getColumnIndex(COLUMN_DESTINATION)),
				res.getString(res.getColumnIndex(COLUMN_MESSAGE)),
				res.getString(res.getColumnIndex(COLUMN_MESSAGE_ID)),
				res.getLong(res.getColumnIndex(COLUMN_TIMESTAMP)),
				res.getString(res.getColumnIndex(COLUMN_CLIENT)),
				res.getString(res.getColumnIndex(COLUMN_ZONE)),
				res.getInt(res.getColumnIndex(COLUMN_SENT)) == 1));
			
			res.moveToNext();
		}
		res.close();
		db.close();
		if (reverse) {
			Collections.reverse(messagesList);
		}
		
		return messagesList;
	}
	
	/**
	 * Deletes the messages from old zones (not your current)
	 */
	public void deleteOldZoneMessages() {
		final String currentZone = Preferences.getString(mContext, Const.sp.CURRENT_ZONE, "");
		
		SQLiteDatabase db = this.getWritableDatabase();
		if (currentZone.isEmpty()) {
			db.delete(ZONE_MESSAGES_TABLE, null, null);
		} else {
			db.delete(ZONE_MESSAGES_TABLE, COLUMN_ZONE + " NOT LIKE '" + currentZone + "'", null);
		}
		db.close();
	}
	
	/**
	 * Deletes all the messages
	 */
	public void deleteZoneMessages() {
		SQLiteDatabase db = this.getReadableDatabase();
		db.execSQL("DROP TABLE IF EXISTS " + ZONE_MESSAGES_TABLE);
		db.close();
	}
}
