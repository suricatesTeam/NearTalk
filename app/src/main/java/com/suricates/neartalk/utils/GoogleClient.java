package com.suricates.neartalk.utils;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;

/**
 * A helper class to  get a GoogleApiClient,
 * it has a singleton instance
 */
public class GoogleClient implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
	
	private static GoogleApiClient mGoogleApiClient;
	
	/**
	 * Creates an instance
	 *
	 * @param context the app context
	 * @return the instance or a new one if it had not been created
	 */
	public static synchronized GoogleApiClient instance(@NonNull Context context) {
		if (mGoogleApiClient == null) {
			mGoogleApiClient = new GoogleClient().create(context);
			mGoogleApiClient.connect();
		}
		return mGoogleApiClient;
	}
	
	/**
	 * Removes the current instance, so a new one can be created
	 */
	public static synchronized void destroy() {
		if (mGoogleApiClient != null) {
			logout();
			mGoogleApiClient.disconnect();
			mGoogleApiClient = null;
		}
	}
	
	private GoogleClient() {
	}
	
	/**
	 * Creates a GoogleApiClient with the callbacks and APIs set
	 *
	 * @param context the app context
	 * @return a GoogleApiClient object
	 */
	private GoogleApiClient create(Context context) {
		GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
			.requestEmail()
			.build();
		mGoogleApiClient = new GoogleApiClient.Builder(context)
			.addApi(LocationServices.API)
			.addApi(Auth.GOOGLE_SIGN_IN_API, gso)
			.addConnectionCallbacks(this)
			.addOnConnectionFailedListener(this)
			.build();
		return mGoogleApiClient;
	}
	
	/**
	 * Logs out from google
	 */
	public static void logout() {
		if (mGoogleApiClient.isConnected()) {
			Auth.GoogleSignInApi.signOut(mGoogleApiClient);
			Auth.GoogleSignInApi.revokeAccess(mGoogleApiClient);
		}
	}
	
	@Override
	public void onConnected(@Nullable Bundle bundle) {
		SLog.d("GoogleApiClient | Connected");
	}
	
	@Override
	public void onConnectionSuspended(int i) {
		SLog.d("GoogleApiClient | ConnectionSuspended");
	}
	
	@Override
	public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
		SLog.d("GoogleApiClient | ConnectionFailed: " + connectionResult.getErrorMessage());
	}
}
