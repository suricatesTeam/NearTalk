package com.suricates.neartalk.utils;

import android.util.Log;

/**
 * Helper class to show logs without the need to pass a tag,
 * uses a default instead
 */
public class SLog {
	
	private static final String TAG = "com.suricates.logging";
	private static boolean showLogs = false;
	
	private SLog() {
	}
	
	/**
	 * Send a debug log.
	 *
	 * @param message the message
	 */
	public static void d(String message) {
		if (showLogs) Log.d(TAG, message);
	}
	
	/**
	 * Send an error log.
	 *
	 * @param message the error
	 */
	public static void e(String message) {
		if (showLogs) Log.e(TAG, message);
	}
	
	/**
	 * Send an info log.
	 *
	 * @param message the message
	 */
	public static void i(String message) {
		if (showLogs) Log.i(TAG, message);
	}
	
	/**
	 * Send a verbose log.
	 *
	 * @param message the message
	 */
	public static void v(String message) {
		if (showLogs) Log.v(TAG, message);
	}
	
	/**
	 * Send a warning log.
	 *
	 * @param message the message
	 */
	public static void w(String message) {
		if (showLogs) Log.w(TAG, message);
	}
	
	/**
	 * Reports something that should never happen
	 *
	 * @param message the message
	 */
	public static void wtf(String message) {
		if (showLogs) Log.wtf(TAG, message);
	}
}