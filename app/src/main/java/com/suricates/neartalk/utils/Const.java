package com.suricates.neartalk.utils;

/**
 * Helper class with constants for the app
 */
public final class Const {
	
	/**
	 * Intent actions
	 */
	public static class actions {
		public static final String UPDATE_BACKGROUND = "com.suricates.neartalk.UPDATE_BACKGROUND_ACTION";
		public static final String UPDATE_USERNAME = "com.suricates.neartalk.UPDATE_USERNAME_ACTION";
		public static final String UPDATE_LOCATION = "com.suricates.neartalk.UPDATE_LOCATION_ACTION";
		public static final String UPDATE_SHOW_USERNAME = "com.suricates.neartalk.UPDATE_SHOW_USERNAME";
		public static final String ROOM_UPDATED = "com.suricates.neartalk.ROOM_UPDATED";
		public static final String ZONE_CHANGED = "com.suricates.neartalk.ZONE_CHANGED";
		public static final String STOP_SERVICE = "com.suricates.neartalk.STOP_SERVICE";
		public static final String LOC_MODE_CHANGED = "android.location.MODE_CHANGED";
		public static final String REBIND_SERVICE = "com.suricates.neartalk.REBIND_SERVICE";
	}
	
	/**
	 * Constants for the messages list
	 */
	public static class messages {
		public static final int OFFSET = 50;
		public static final int PRELOAD_LIMIT = 15;
	}
	
	/**
	 * Tabs index
	 */
	public static class tab {
		public static final int SETTINGS = 0;
		public static final int ZONE_CHAT = 1;
		public static final int LOCATION = 2;
	}
	
	/**
	 * Request codes for intents
	 */
	public static class codes {
		public static final int RINGTONE = 1;
		public static final int RC_SIGN_IN = 2;
		public static final int CHAT_BG = 3;
	}
	
	/**
	 * SharedPreferences keys
	 */
	public static class sp {
		public static final String RINGTONE = "com.suricates.neartalk.CURRENT_RINGTONE";
		public static final String CHAT_BG = "com.suricates.neartalk.CHAT_BG";
		public static final String USER_ID = "com.suricates.neartalk.USER_UNIQUE_ID";
		public static final String USER_NAME = "com.suricates.neartalk.USER_UNIQUE_USER_NAME";
		public static final String CURRENT_ZONE = "com.suricates.neartalk.CURRENT_ZONE";
		public static final String GLOBAL_CHAT_VISIBLE = "com.suricates.neartalk.GLOBAL_CHAT_VISIBLE";
		public static final String CURR_LAT = "com.suricates.neartalk.CURR_LAT";
		public static final String CURR_LON = "com.suricates.neartalk.CURR_LON";
		public static final String CURR_ZONE_LAT = "com.suricates.neartalk.CURR_ZONE_LAT";
		public static final String CURR_ZONE_LON = "com.suricates.neartalk.CURR_ZONE_LON";
		public static final String CURR_ZONE_USERS = "com.suricates.neartalk.CURR_ZONE_USERS";
		public static final String SEPARATOR = "&%!";
		public static final String IS_GPS_ENABLED = "com.suricates.neartalk.IS_GPS_ENABLED";
		public static final String IS_NETWORK_ENABLED = "com.suricates.neartalk.IS_NETWORK_ENABLED";
	}
	
	/**
	 * JSON fields of the requests returned json files
	 */
	public static class json {
		public static final String POSITION = "position";
		public static final String LOCATION = "location";
		public static final String COORDINATES = "coordinates";
		public static final String NAME = "name";
		public static final String EMAIL = "email";
		public static final String ERR = "error";
		public static final String ROOM_ID = "ntzoneid";
		public static final String USER_ID = "ntid";
		public static final String ROOM_USERS = "users";
	}
	
	/**
	 * KEYS for the MQTTService and Client
	 */
	public static class mqtt {
		//public static final String TCP = "ws://broker.hivemq.com:8000";
		public static final String TCP = "tcp://broker.hivemq.com:1883";
		public static final String TOPIC = "neartalk-mqtt";
		
		public static final int SERVICE_BINDER_MESSAGE_WHAT = 6;
		public static final int SERVICE_OUTGOING_MESSAGE_WHAT = 8;
		public static final int SERVICE_INCOMING_MESSAGE_WHAT = 10;
		public static final int SERVICE_GLOBAL_CONNECTED_WHAT = 12;
		public static final int SERVICE_CHANGE_ZONE_WHAT = 14;
		public static final int SERVICE_NO_GPS_WHAT = 16;
		public static final int SERVICE_NO_NETWORK_WHAT = 17;
		public static final int SERVICE_GLOBAL_STOPPED_WHAT = 18;
		public static final int SERVICE_WAITING_GPS_WHAT = 20;
		public static final String SERVICE_INCOMING_MESSAGE_BUNDLE_KEY = "com.suricates.neartalk.MESSAGE_BUNDLE_IN";
		public static final String SERVICE_OUTGOING_MESSAGE_BUNDLE_KEY = "com.suricates.neartalk.MESSAGE_BUNDLE_OUT";
	}
	
	/**
	 * FloatingActionButton actions and keys
	 */
	public static class fab {
		public static final String SCROLL_MESSAGES_ACTION = "com.suricates.neartalk.SCROLL_MESSAGES_ACTION";
		public static final String CLICK_ACTION = "com.suricates.neartalk.CLICK_ACTION";
		public static final String SCROLL_MESSAGES_KEY = "com.suricates.neartalk.SCROLL_MESSAGES_KEY";
	}
	
	/**
	 * Static values of the notification
	 */
	public static class notification {
		public static final int ID = 12423;
		static final int LIMIT_MESSAGES = 5;
		public static final int ID_NO_GPS = 272829;
		public static final int ID_NO_NETWORK = 66598;
		
	}
	
	/**
	 * API error codes, used for parsing  into text
	 */
	public static class errors {
		public static final String EMAIL_TAKEN = "598234679";
		public static final String NAME_TAKEN = "524532455";
	}
	
	private Const() {
	}
}