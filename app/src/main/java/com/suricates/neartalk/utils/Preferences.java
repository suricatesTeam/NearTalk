package com.suricates.neartalk.utils;

import android.content.Context;

import net.grandcentrix.tray.AppPreferences;

/**
 * Helper class to store the app preferences
 */
public final class Preferences {
	
	/**
	 * Sets a value for the given preference
	 *
	 * @param context the app context
	 * @param key     the preference key
	 * @param value   the preference new value
	 */
	public static void set(final Context context, String key, Object value) {
		AppPreferences appPreferences = new AppPreferences(context);
		if (value instanceof Integer) {
			appPreferences.put(key, (int) value);
		} else if (value instanceof String) {
			appPreferences.put(key, (String) value);
		} else if (value instanceof Long) {
			appPreferences.put(key, (long) value);
		} else if (value instanceof Float) {
			appPreferences.put(key, (float) value);
		} else if (value instanceof Boolean) {
			appPreferences.put(key, (boolean) value);
		} else {
			SLog.e("Preferences: Can't save that value, wrong type: " + value.getClass());
		}
	}
	
	/**
	 * Gets an int from the preferences
	 *
	 * @param context      the app context
	 * @param key          the preference key
	 * @param defaultValue a default value it is not found
	 * @return the value or the default if not found
	 */
	public static int getInt(final Context context, String key, int defaultValue) {
		return new AppPreferences(context).getInt(key, defaultValue);
	}
	
	/**
	 * Gets a string from the preferences
	 *
	 * @param context      the app context
	 * @param key          the preference key
	 * @param defaultValue a default value it is not found
	 * @return the value or the default if not found
	 */
	public static String getString(final Context context, String key, String defaultValue) {
		return new AppPreferences(context).getString(key, defaultValue);
	}
	
	/**
	 * Gets a long from the preferences
	 *
	 * @param context      the app context
	 * @param key          the preference key
	 * @param defaultValue a default value it is not found
	 * @return the value or the default if not found
	 */
	public static long getLong(final Context context, String key, long defaultValue) {
		return new AppPreferences(context).getLong(key, defaultValue);
	}
	
	/**
	 * Gets a float from the preferences
	 *
	 * @param context      the app context
	 * @param key          the preference key
	 * @param defaultValue a default value it is not found
	 * @return the value or the default if not found
	 */
	public static float getFloat(final Context context, String key, float defaultValue) {
		return new AppPreferences(context).getFloat(key, defaultValue);
	}
	
	/**
	 * Gets a boolean from the preferences
	 *
	 * @param context      the app context
	 * @param key          the preference key
	 * @param defaultValue a default value it is not found
	 * @return the value or the default if not found
	 */
	public static boolean getBoolean(final Context context, String key, boolean defaultValue) {
		return new AppPreferences(context).getBoolean(key, defaultValue);
	}
	
	/**
	 * Removes a preference
	 *
	 * @param context the app context
	 * @param key     the preference to remove
	 */
	public static void remove(final Context context, String key) {
		new AppPreferences(context).remove(key);
	}
}
