package com.suricates.neartalk.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.suricates.neartalk.http.AsyncTaskManager;
import com.suricates.neartalk.utils.U;

/**
 * Receiver used for to know when the device is shutting down
 */
public class ShutdownReceiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		new AsyncTaskManager(context).disconnectUser();
		U.close(context);
	}
}
