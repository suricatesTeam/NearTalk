package com.suricates.neartalk.utils;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.suricates.neartalk.LoginActivity;
import com.suricates.neartalk.MainActivity;
import com.suricates.neartalk.R;
import com.suricates.neartalk.http.AsyncTaskManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * This is a helper class with methods that can be used from any other class,
 * and avoid repeating code
 */
public final class U {
	
	private static NotificationCompat.InboxStyle inboxStyle;
	private static NotificationCompat.Builder notification;
	
	/**
	 * Checks whether the app has internet connection
	 *
	 * @param context the app context
	 * @return true if it has internet, false otherwise
	 */
	public static boolean hasInternet(final Context context) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo nInfo = cm.getActiveNetworkInfo();
		return nInfo != null && nInfo.isConnected();
	}
	
	/**
	 * Sends a toast
	 *
	 * @param msg     the message to show
	 * @param context the app context
	 */
	public static void sendToast(@StringRes int msg, Context context) {
		Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
	}
	
	/**
	 * Launches an intent to the gallery to pick a photo
	 *
	 * @param activity    the current activity
	 * @param requestCode a request code to handle the return
	 */
	public static void intentPickPhoto(Activity activity, int requestCode) {
		activity.startActivityForResult(
			new Intent(
				Intent.ACTION_PICK,
				MediaStore.Images.Media.EXTERNAL_CONTENT_URI
			),
			requestCode
		);
	}
	
	/**
	 * Helper method to send a notification, applying the preferences values
	 *
	 * @param context      the app context
	 * @param id           the notification id
	 * @param notification the notification to send
	 */
	private static void sendNotification(Context context, int id, @NonNull Notification notification) {
		SLog.d("Sound enabled? " + Preferences.getBoolean(context, context.getString(R.string.switch_notification_sound_key), true));
		if (!Preferences.getBoolean(context, context.getString(R.string.switch_notification_sound_key), true)) {
			notification.sound = null;
		}
		SLog.d("Notification sound: " + notification.sound);
		((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE))
			.notify(id, notification);
	}
	
	/**
	 * Cancels all the active notifications
	 *
	 * @param context the app context
	 */
	public static void cancelAllNotifications(Context context) {
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(Const.notification.ID_NO_GPS);
		notificationManager.cancel(Const.notification.ID_NO_NETWORK);
		notificationManager.cancel(Const.notification.ID);
	}
	
	/**
	 * Sends a notification with the unread messages
	 *
	 * @param context the app context
	 */
	public static void pushNotification(Context context) {
		if (!Preferences.getBoolean(context, Const.sp.GLOBAL_CHAT_VISIBLE, true)) {
			inboxStyle = new NotificationCompat.InboxStyle();
			int num = new DBHelper(context).getUnreadMessages(inboxStyle);
            String text = context.getString(R.string.you_have) + " " + context.getResources()
                    .getQuantityString(R.plurals.messages_to_read, num, num);
			sendNotification(
				context,
				Const.notification.ID,
				getNotification(context)
					.setContentText(text)
					.setStyle(inboxStyle)
					.setSound(getNotificationSound(context))
					.build()
			);
		}
	}
	
	/**
	 * Sends a notification informing of an error (can't be dismissed)
	 *
	 * @param context the app context
	 * @param id      the id used to cancel it programmatically
	 * @param message the message to display
	 */
	public static void sendAlertNotification(Context context, int id, @NonNull String message) {
		NotificationCompat.Builder notification = new NotificationCompat.Builder(context)
			.setContentTitle(context.getString(R.string.chat_service))
			.setContentText(message)
			.setSmallIcon(R.drawable.ic_alert)
			.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
			.setSound(getNotificationSound(context))
			.setAutoCancel(true);
		
		switch (id) {
			case Const.notification.ID_NO_GPS:
				notification.addAction(
					new NotificationCompat.Action.Builder(
						R.drawable.ic_location,
						context.getString(R.string.enable_gps),
						PendingIntent.getActivity(
							context,
							0,
							new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS),
							0)
					).build());
				notification.addAction(
					new NotificationCompat.Action.Builder(
						R.drawable.ic_disable_app,
						context.getString(R.string.disable_app),
						PendingIntent.getBroadcast(
							context,
							12345,
							new Intent(Const.actions.STOP_SERVICE),
							PendingIntent.FLAG_UPDATE_CURRENT)
					).build());
				break;
			case Const.notification.ID_NO_NETWORK:
				notification.addAction(
					new NotificationCompat.Action.Builder(
						R.drawable.ic_network,
						context.getString(R.string.enable_network),
						PendingIntent.getActivity(
							context,
							0,
							new Intent(Settings.ACTION_DATA_ROAMING_SETTINGS),
							0)
					).build());
				notification.addAction(
					new NotificationCompat.Action.Builder(
						R.drawable.ic_wifi,
						context.getString(R.string.enable_wifi),
						PendingIntent.getActivity(
							context,
							0,
							new Intent(Settings.ACTION_WIFI_SETTINGS),
							0)
					).build());
				break;
			default:
				SLog.e("sendAlertNotification() | Should not get here");
				break;
		}
		
		sendNotification(context, id, notification.build());
	}
	
	/**
	 * Helper method to get a Notification.Builder instance
	 *
	 * @param context the app context
	 * @return a Notification.Builder instance
	 */
	private static NotificationCompat.Builder getNotification(Context context) {
		if (notification == null) {
			notification = new NotificationCompat.Builder(context)
				.setSmallIcon(R.drawable.ic_chat)
				.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_notification))
				.setContentTitle(context.getString(R.string.global_chat))
				.setAutoCancel(true)
				.setContentIntent(
					PendingIntent.getActivity(
						context,
						0,
						new Intent(context, MainActivity.class),
						PendingIntent.FLAG_CANCEL_CURRENT)
				);
		}
		return notification;
	}
	
	/**
	 * Gets the notification sound chosen by the user, if not set, the default one
	 *
	 * @param context the app context
	 * @return an uri to the ringtone
	 */
	private static Uri getNotificationSound(Context context) {
		String resource = Preferences.getString(context, Const.sp.RINGTONE, null);
		return resource != null ? Uri.parse(resource) : RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
	}
	
	/**
	 * Parses an URI to an absolute oath
	 *
	 * @param context the app context
	 * @param uri     the URI to parse
	 * @return the URI parsed
	 */
	public static String absolutePathFromUri(Context context, Uri uri) {
		Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
		if (cursor == null) {
			return uri.getPath();
		}
		cursor.moveToFirst();
		try {
			String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA));
			cursor.close();
			return path;
		} catch (Exception ignored) {
		}
		return null;
	}
	
	/**
	 * Checks if it is a valid email
	 *
	 * @param email the email to check
	 * @return true if it is valid, false otherwise
	 */
	public static boolean isValidEmail(String email) {
		return !(email == null || email.isEmpty()) && Pattern.compile(
			"^[_A-Za-z0-9-+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"
		).matcher(email).find();
	}
	
	/**
	 * Checks if it is a valid name
	 *
	 * @param name the name to check
	 * @return true if it is valid, false otherwise
	 */
	public static boolean isValidName(String name) {
		return !(name == null || name.isEmpty()) &&
			Pattern.compile("^[_A-Za-z0-9-]{4,12}$").matcher(name).find();
	}
	
	/**
	 * Hides the keyboard
	 *
	 * @param activity the current activity
	 */
	public static void hideKeyboard(Activity activity) {
		InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(activity.findViewById(android.R.id.content).getWindowToken(), 0);
	}
	
	/**
	 * Shows the keyboard focused on a view
	 *
	 * @param context the app context
	 * @param view    the view where to focus the keyboard
	 */
	public static void showKeyboard(Context context, View view) {
		InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
	}
	
	/**
	 * Shows a dialog displaying an error message
	 *
	 * @param context the app context
	 * @param message the error message
	 */
	public static void showErrorDialog(Context context, @StringRes int message) {
		new AlertDialog.Builder(context, R.style.DialogTheme)
			.setIcon(R.drawable.ic_alert_grey)
			.setTitle(R.string.error)
			.setMessage(message)
			.setPositiveButton(R.string.ok, (dialog, which) -> dialog.dismiss())
			.create().show();
	}
	
	/**
	 * Shows a confirmation dialog
	 *
	 * @param context  the app context
	 * @param listener what to do when it is confirmed
	 */
	public static void confirmDialog(Context context, DialogInterface.OnClickListener listener) {
		new android.support.v7.app.AlertDialog.Builder(context, R.style.DialogTheme)
			.setTitle(R.string.are_sure)
			.setCancelable(false)
			.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss())
			.setPositiveButton(R.string.ok, listener)
			.create().show();
	}
	
	/**
	 * Shows a confirmation dialog
	 *
	 * @param context the app context
	 * @param message the message to show
	 * @param ok      what to do when it is confirmed
	 */
	public static void warningDialog(Context context, @StringRes int message, DialogInterface.OnClickListener ok) {
		new android.support.v7.app.AlertDialog.Builder(context, R.style.DialogTheme)
			.setTitle(R.string.are_sure)
			.setMessage(message)
			.setIcon(R.drawable.ic_alert_grey)
			.setCancelable(false)
			.setNegativeButton(R.string.cancel, (dialog, which) -> dialog.dismiss())
			.setPositiveButton(R.string.ok, ok)
			.create().show();
	}
	
	/**
	 * Logs out from the app (resets the values)
	 *
	 * @param context the app context
	 */
	public static void logout(Context context) {
		SLog.i("Logging out...");
		
		context.sendBroadcast(new Intent(Const.actions.STOP_SERVICE));
		new AsyncTaskManager(context).disconnectUser();
		new DBHelper(context).dropUnReadMessagesTrigger();
		Preferences.remove(context, Const.sp.USER_ID);
		Preferences.remove(context, Const.sp.USER_NAME);
		Preferences.set(context, context.getString(R.string.global_chat_listener_key), false);
		close(context);
		
		Activity activity = (Activity) context;
		activity.overridePendingTransition(0, 0);
		activity.startActivity(
			new Intent(activity, LoginActivity.class),
			ActivityOptions.makeCustomAnimation(
				activity,
				android.R.anim.slide_in_left,
				android.R.anim.slide_out_right
			).toBundle()
		);
		activity.finish();
	}
	
	/**
	 * Closes the 'connection' to the zone
	 *
	 * @param context the app context
	 */
	public static void close(Context context) {
		Preferences.remove(context, Const.sp.CURRENT_ZONE);
		Preferences.remove(context, Const.sp.CURR_LAT);
		Preferences.remove(context, Const.sp.CURR_LON);
		Preferences.remove(context, Const.sp.CURR_ZONE_LAT);
		Preferences.remove(context, Const.sp.CURR_ZONE_LON);
	}
	
	/**
	 * Parses a timestamp
	 *
	 * @param microseconds the timestamp
	 * @return the timestamp in string format
	 */
	public static String formatToTime(long microseconds) {
		return SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT).format(new Date(microseconds));
	}
	
	/**
	 * Unregister a broadcast receiver
	 *
	 * @param context           the app context
	 * @param broadcastReceiver the BroadcastReceiver to unregister
	 */
	public static void silentlyUnregisterReceiver(Context context, BroadcastReceiver broadcastReceiver) {
		try {
			context.unregisterReceiver(broadcastReceiver);
		} catch (IllegalArgumentException ignored) {
		}
	}
	
	private U() {
	}
}
