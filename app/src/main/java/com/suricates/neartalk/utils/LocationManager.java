package com.suricates.neartalk.utils;

import android.content.Context;
import android.location.Location;
import android.os.CountDownTimer;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.suricates.neartalk.http.AsyncTaskManager;

/**
 * A helper class to update the users location, using a singleton instance
 */
public class LocationManager implements LocationListener {
	
	private static LocationManager mLocationManager;
	
	private GoogleApiClient mGoogleApiClient;
	private LocationRequest mLocationRequest;
	private AsyncTaskManager mAsyncTaskManager;
	
	/**
	 * Creates an instance
	 *
	 * @param context the app context
	 * @return the instance or a new one if it had not been created
	 */
	public static synchronized LocationManager instance(Context context) {
		if (mLocationManager == null) {
			mLocationManager = new LocationManager(context);
		}
		return mLocationManager;
	}
	
	/**
	 * Removes the current instance, so a new one can be created
	 */
	public static synchronized void destroy() {
		if (mLocationManager != null) {
			mLocationManager.onDestroy();
			mLocationManager = null;
		}
	}
	
	/**
	 * Creates the instance an configures the listeners and callbacks
	 *
	 * @param context the app context
	 */
	private LocationManager(Context context) {
		mGoogleApiClient = GoogleClient.instance(context);
		
		mLocationRequest = LocationRequest.create();
		mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
		mLocationRequest.setFastestInterval(30000);
		mLocationRequest.setInterval(60000);
		
		mAsyncTaskManager = new AsyncTaskManager(context);
		
		new CountDownTimer(5000, 500) {
			boolean done = false;
			
			@Override
			public void onTick(long l) {
				if (!done && mGoogleApiClient.isConnected()) {
					try {
						LocationServices.FusedLocationApi.requestLocationUpdates(
							mGoogleApiClient, mLocationRequest, LocationManager.this
						);
						Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
						if (lastLocation != null) onLocationChanged(lastLocation);
						done = true;
					} catch (SecurityException ignored) {
					}
				} else if (!done) {
					SLog.i("Waiting for the google client to connect...");
				}
			}
			
			@Override
			public void onFinish() {
			}
		}.start();
	}
	
	@Override
	public void onLocationChanged(Location location) {
		SLog.i("Location changed: {" + location.getLatitude() + ", " + location.getLongitude() + "}");
		mAsyncTaskManager.updatePosition(location.getLatitude(), location.getLongitude());
	}
	
	/**
	 * Removes this as the location update listener
	 */
	private void onDestroy() {
		if (mGoogleApiClient.isConnected()) {
			LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, LocationManager.this);
		}
	}
}
