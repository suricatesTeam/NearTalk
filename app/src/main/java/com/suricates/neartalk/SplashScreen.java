package com.suricates.neartalk;

import android.Manifest;
import android.app.ActivityOptions;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.suricates.neartalk.utils.Const;
import com.suricates.neartalk.utils.Preferences;
import com.suricates.neartalk.utils.U;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This class basically is a screen to ask for permissions to the user, and go to the app if those are accepted,
 * if not, finishes the app
 */
public class SplashScreen extends AppCompatActivity {
	
	private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1234;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_screen);
		
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
			intent();
		} else {
			final String[] permissions = new String[]{
				Manifest.permission.WRITE_EXTERNAL_STORAGE,
				Manifest.permission.ACCESS_FINE_LOCATION,
				Manifest.permission.ACCESS_COARSE_LOCATION
			};
			
			if (!hasPermissions(this, permissions)) {
				ActivityCompat.requestPermissions(this, permissions, REQUEST_ID_MULTIPLE_PERMISSIONS);
			} else {
				intent();
			}
		}
	}
	
	/**
	 * Checks whether the user has granted all the permission
	 *
	 * @param context     the app context
	 * @param permissions an array with all the needed permissions
	 * @return true if it has granted all the permissions, false otherwise
	 */
	private boolean hasPermissions(Context context, String... permissions) {
		for (String permission : permissions) {
			if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults) {
		switch (requestCode) {
			case REQUEST_ID_MULTIPLE_PERMISSIONS:
				if (grantResults.length > 0) {
					for (int result : grantResults) {
						if (result != PackageManager.PERMISSION_GRANTED) {
							U.sendToast(R.string.permissions_needed, SplashScreen.this);
							finish();
							return;
						}
					}
					intent();
				}
				break;
			default:
				super.onRequestPermissionsResult(requestCode, permissions, grantResults);
				break;
		}
	}
	
	/**
	 * Launches an intent to the corresponding class, MainActivity or LoginActivity if not logged
	 */
	private void intent() {
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				Class where;
				if (Preferences.getString(SplashScreen.this, Const.sp.USER_ID, null) == null &&
					Preferences.getString(SplashScreen.this, Const.sp.USER_NAME, null) == null) {
					where = LoginActivity.class;
				} else {
					where = MainActivity.class;
				}
				Intent login = new Intent(SplashScreen.this, where);
				Bundle options = ActivityOptions.makeCustomAnimation(
					SplashScreen.this,
					R.anim.fade_in,
					R.anim.fade_out
				).toBundle();
				startActivity(login, options);
				finish();
			}
		}, 1000);
	}
}
