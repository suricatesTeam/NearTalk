package com.suricates.neartalk;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.os.AsyncTask;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.suricates.neartalk.http.AsyncTaskManager;
import com.suricates.neartalk.utils.Const;
import com.suricates.neartalk.utils.GoogleClient;
import com.suricates.neartalk.utils.U;

import java.util.Timer;
import java.util.TimerTask;

/**
 * This class is only used to make the user login to the app and start the app
 */
public class LoginActivity extends AppCompatActivity implements AsyncTaskManager.OnTaskCompletedListener {
	
	private UserLoginTask mAuthTask = null;
	
	private EditText mUsername;
	private View mProgressView;
	private View mLoginFormView;
	
	private boolean registerForm = false;
	
	private GoogleApiClient mGoogleApiClient;
	
	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		mUsername = (EditText) findViewById(R.id.username);
		final ScrollView scrollView = (ScrollView) findViewById(R.id.scroll_login_form);
		mLoginFormView = findViewById(R.id.email_login_form);
		mProgressView = findViewById(R.id.login_progress);
		ProgressBar bar = (ProgressBar) mProgressView;
		if (bar.getIndeterminateDrawable() != null) {
			bar.getIndeterminateDrawable().setColorFilter(getResources().getColor(R.color.colorAccent), PorterDuff.Mode.SRC_IN);
		}
		final TextView mRegisterSwitch = (TextView) findViewById(R.id.register_switch);
		final Button mButton = (Button) findViewById(R.id.email_sign_in_button);
		
		mButton.setOnClickListener(view -> signIn());
		mRegisterSwitch.setText(R.string.dont_have_account);
		mRegisterSwitch.setOnClickListener(v -> {
			if (!registerForm) {
				mRegisterSwitch.setText(R.string.have_account);
				mButton.setText(R.string.action_register);
				mUsername.setVisibility(View.VISIBLE);
				mUsername.requestFocusFromTouch();
				scrollView.post(() -> new Timer().schedule(new TimerTask() {
					@Override
					public void run() {
						scrollView.smoothScrollTo(0, scrollView.getHeight());
					}
				}, 200));
				U.showKeyboard(getApplicationContext(), mUsername);
				registerForm = true;
			} else {
				mRegisterSwitch.setText(R.string.dont_have_account);
				mButton.setText(R.string.action_sign_in);
				mUsername.setVisibility(View.GONE);
				U.hideKeyboard(LoginActivity.this);
				registerForm = false;
			}
		});
		
		mGoogleApiClient = GoogleClient.instance(this);
	}
	
	/**
	 * SignIn using google
	 */
	private void signIn() {
		startActivityForResult(Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient), Const.codes.RC_SIGN_IN);
	}
	
	/**
	 * Error check before attempting the real login
	 *
	 * @param email the users email
	 */
	private void attemptLogin(String email) {
		if (mAuthTask != null) {
			return;
		}
		
		mUsername.setError(null);
		
		boolean cancel = false;
		View focusView = null;
		
		String username = null;
		if (mUsername.getVisibility() == View.VISIBLE) {
			username = mUsername.getText().toString();
			
			if (TextUtils.isEmpty(username)) {
				mUsername.setError(getString(R.string.error_field_required));
				focusView = mUsername;
				cancel = true;
			}
		}
		
		if (cancel) {
			focusView.requestFocus();
		} else {
			showProgress(true);
			mAuthTask = new UserLoginTask(username, email);
			mAuthTask.execute((Void) null);
		}
	}
	
	/**
	 * Helper class to attempt the login to the apps database in a background process
	 */
	private class UserLoginTask extends AsyncTask<Void, Void, Boolean> {
		
		private final String mUsername;
		private final String mEmail;
		
		private UserLoginTask(String username, String email) {
			mUsername = username;
			mEmail = email;
		}
		
		@Override
		protected Boolean doInBackground(Void... params) {
			if (registerForm) {
				return new AsyncTaskManager(LoginActivity.this).register(mEmail, mUsername);
			} else {
				return new AsyncTaskManager(LoginActivity.this).login(mEmail);
			}
		}
		
		@Override
		protected void onPostExecute(Boolean success) {
			mAuthTask = null;
			if (!success) {
				showProgress(false);
				GoogleClient.logout();
			}
		}
		
		@Override
		protected void onCancelled() {
			mAuthTask = null;
		}
	}
	
	/**
	 * Shows a progress bar
	 *
	 * @param show true to show the progressbar, false to hide it
	 */
	private void showProgress(final boolean show) {
		int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);
		
		mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
		mLoginFormView.animate().setDuration(shortAnimTime).alpha(
			show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
			}
		});
		
		mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
		mProgressView.animate().setDuration(shortAnimTime).alpha(
			show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
			@Override
			public void onAnimationEnd(Animator animation) {
				mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
			}
		});
	}
	
	/**
	 * Launches an intent to the MainActivity
	 */
	private void intent() {
		Intent main = new Intent(this, MainActivity.class);
		Bundle options = ActivityOptions.makeCustomAnimation(
			this, R.anim.fade_in, R.anim.fade_out
		).toBundle();
		startActivity(main, options);
		finish();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == Const.codes.RC_SIGN_IN) {
			GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
			if (result.isSuccess()) {
				GoogleSignInAccount account = result.getSignInAccount();
				if (account != null) {
					attemptLogin(account.getEmail());
				}
			}
		}
	}
	
	@Override
	public void onTaskCompleted(@StringRes int error) {
		if (error != -1) {
			showProgress(false);
			GoogleClient.logout();
			U.showErrorDialog(LoginActivity.this, error);
			return;
		}
		
		GoogleClient.destroy();
		intent();
	}
}
