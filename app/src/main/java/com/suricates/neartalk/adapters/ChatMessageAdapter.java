package com.suricates.neartalk.adapters;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.suricates.neartalk.R;
import com.suricates.neartalk.adapters.models.ChatMessage;
import com.suricates.neartalk.client.models.Message;
import com.suricates.neartalk.utils.Const;
import com.suricates.neartalk.utils.DBHelper;
import com.suricates.neartalk.utils.U;

import java.util.LinkedList;

/**
 * Adapter for the messages
 */
public class ChatMessageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
	
	private final Context mContext;
	
	private final LinearLayoutManager mLayoutManager;
	private final String mClientID;
	private LinkedList<ChatMessage> chatMessages;
	private final RecyclerView recyclerView;
	
	private boolean mLoadingMessages = false;
	private long mLastTimeStamp = -1;
	private boolean mScrolling = false;
	private boolean showingFabIcon = false;
	
	/**
	 * Creates the adapter and configures the recycler view
	 *
	 * @param context      the app context
	 * @param messages     a list with the messages
	 * @param recyclerView where the messages will be displayed
	 * @param clientID     the current user's ID
	 */
	public ChatMessageAdapter(Context context, @Nullable final LinkedList<ChatMessage> messages, RecyclerView recyclerView, String clientID) {
		mClientID = clientID;
		mContext = context;
		chatMessages = messages != null ? messages : new LinkedList<>();
		this.recyclerView = recyclerView;
		mLayoutManager = new LinearLayoutManager(mContext);
		mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
		mLayoutManager.setStackFromEnd(true);
		
		if (chatMessages.size() > 0)
			mLastTimeStamp = Long.parseLong(chatMessages.getFirst().getTime());
		
		LocalBroadcastManager.getInstance(mContext).registerReceiver(fabClickReceiver, new IntentFilter(Const.fab.CLICK_ACTION));
		
		recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
			@SuppressWarnings("ConstantConditions")//messages will always be not null
			@Override
			public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
				super.onScrolled(recyclerView, dx, dy);
				
				if (messages.size() >= Const.messages.PRELOAD_LIMIT && !mLoadingMessages &&
					mLayoutManager.findFirstCompletelyVisibleItemPosition() <= Const.messages.PRELOAD_LIMIT) {
					mLoadingMessages = true;
					DBHelper dbHelper = new DBHelper(mContext);
					LinkedList<Message> messagesFromZone = dbHelper.getMessagesFromZone(
						Const.messages.OFFSET,
						mLastTimeStamp,
						false
					);
					
					for (Message message : messagesFromZone) {
						messages.addFirst(new ChatMessage(
							message.getOrigin(),
							message.getMessage(),
							String.valueOf(message.getTimestamp()),
							message.getClientID().equalsIgnoreCase(mClientID),
							true,
							message.isSent(),
							message.getMessageID()));
						
						mLastTimeStamp = message.getTimestamp();
					}
					recyclerView.post(() -> {
						notifyItemRangeInserted(0, messagesFromZone.size());
						mLoadingMessages = false;
					});
				}
			}
			
			@SuppressWarnings("ConstantConditions")//messages will always be not null
			@Override
			public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
				super.onScrollStateChanged(recyclerView, newState);
				
				mScrolling = mLayoutManager.findLastVisibleItemPosition() < messages.size() - 1;
				recyclerView.post(() -> switchFabIcon(mScrolling));
				
			}
		});
		
		mContext.registerReceiver(new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				notifyZoneChanged();
			}
		}, new IntentFilter(Const.actions.ZONE_CHANGED));
	}
	
	/**
	 * Prints a 'notification' on the list telling the user that his current zone has changed
	 *
	 * @return true it is successfully added, false otherwise
	 */
	private boolean notifyZoneChanged() {
		return add(new ChatMessage(mContext.getString(R.string.zone_changed), true));
	}
	
	/**
	 * Adds a message to the list
	 *
	 * @param chatMessage the message to add
	 * @return true if the message has been added, false otherwise
	 */
	public boolean add(ChatMessage chatMessage) {
		boolean add = chatMessages.add(chatMessage);
		notifyDataSetChanged();
		recyclerView.post(() -> {
			if (chatMessage.isMine() || !mLoadingMessages && !mScrolling) {
				switchFabIcon(false);
				recyclerView.scrollToPosition(chatMessages.size() - 1);
			}
		});
		return add;
	}
	
	/**
	 * Sets the message status as sent
	 *
	 * @param tag the tag of the message
	 */
	public void setToSent(String tag) {
		View v = recyclerView.findViewWithTag(tag);
		int pos = recyclerView.getChildLayoutPosition(v);
		if (pos != -1) {
			chatMessages.get(pos).setSent(true);
			notifyItemChanged(pos);
		}
	}
	
	/**
	 * Shows or hides the FloatingActionButton
	 *
	 * @param show true to show it, false otherwise
	 */
	private void switchFabIcon(boolean show) {
		//Prevent sending the same intent again
		if (showingFabIcon != show) {
			showingFabIcon = show;
			Intent intent = new Intent(Const.fab.SCROLL_MESSAGES_ACTION);
			intent.putExtra(Const.fab.SCROLL_MESSAGES_KEY, show);
			LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
		}
	}
	
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		View v = LayoutInflater.from(parent.getContext()).inflate(viewType, parent, false);
		return viewType == R.layout.message_notification ? new NotificationViewHolder(v) : new MessageViewHolder(v);
	}
	
	@Override
	public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
		ChatMessage item = chatMessages.get(position);
		
		if (holder instanceof MessageViewHolder) {
			MessageViewHolder mHolder = (MessageViewHolder) holder;
			if (item.isGlobal()) mHolder.mUsername.setText(item.getUsername().trim());
			
			mHolder.mMessage.setText(item.getMsg().trim());
			mHolder.mTime.setText(U.formatToTime(Long.parseLong(item.getTime().trim())));
			
			if (item.isMine()) {
				if (item.isSent()) {
					mHolder.mSent.setImageResource(R.mipmap.ic_sent);
				} else {
					mHolder.mSent.setImageResource(R.mipmap.ic_to_send);
				}
				
				String msgID = item.getMsgID();
				if (msgID != null)
					mHolder.setTag(msgID);
			}
			return;
		}
		
		((NotificationViewHolder) holder).mMessage.setText(item.getMsg());
	}
	
	@Override
	public int getItemViewType(int pos) {
		ChatMessage item = chatMessages.get(pos);
		
		return item.isNotification() ? R.layout.message_notification :
			item.isMine() ? R.layout.right_chat_bubble_g : R.layout.left_chat_bubble_g;
	}
	
	@Override
	public int getItemCount() {
		return chatMessages.size();
	}
	
	/**
	 * A BroadcastReceiver used to scroll the list of messages to the bottom
	 */
	private final BroadcastReceiver fabClickReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			recyclerView.post(() -> {
				if (chatMessages.size() > Const.messages.PRELOAD_LIMIT &&
					(Const.messages.OFFSET - mLayoutManager.findFirstVisibleItemPosition()) > Const.messages.PRELOAD_LIMIT) {
					recyclerView.scrollToPosition(chatMessages.size() - Const.messages.PRELOAD_LIMIT);
				}
				recyclerView.smoothScrollToPosition(chatMessages.size() - 1);
				mScrolling = false;
				switchFabIcon(false);
			});
		}
	};
	
	/**
	 * Unregisters the BroadcastReceiver used to scroll the list
	 */
	public void unregisterFabReceiver() {
		U.silentlyUnregisterReceiver(mContext, fabClickReceiver);
	}
	
	/**
	 * Gets the LayoutManager for the RecyclerView of messages
	 *
	 * @return the LayoutManager
	 */
	public LinearLayoutManager getLayoutManager() {
		return mLayoutManager;
	}
	
	/**
	 * A ViewHolder used to setup the RecyclerView items
	 */
	private class MessageViewHolder extends RecyclerView.ViewHolder {
		
		TextView mUsername;
		TextView mMessage;
		TextView mTime;
		ImageView mSent;
		View view;
		
		/**
		 * Sets up the view used to display the new item on the list
		 *
		 * @param view the view to setup
		 */
		MessageViewHolder(View view) {
			super(view);
			
			this.view = view;
			mUsername = (TextView) view.findViewById(R.id.usernameChatBubble);
			mMessage = (TextView) view.findViewById(R.id.textChatBubble);
			mTime = (TextView) view.findViewById(R.id.timeChatBuuble);
			mSent = (ImageView) view.findViewById(R.id.statusBubbleChat);
		}
		
		void setTag(String tag) {
			view.setTag(tag);
		}
	}
	
	/**
	 * A ViewHolder used to setup the RecyclerView chat notifications
	 */
	private class NotificationViewHolder extends RecyclerView.ViewHolder {
		
		TextView mMessage;
		
		/**
		 * Sets up the view used to display the 'notification' on the list
		 *
		 * @param view the view to setup
		 */
		NotificationViewHolder(View view) {
			super(view);
			mMessage = (TextView) view.findViewById(R.id.message_notification_text);
		}
	}
}