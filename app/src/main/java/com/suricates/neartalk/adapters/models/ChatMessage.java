package com.suricates.neartalk.adapters.models;

/**
 * POJO class of the message that appears on the chat screen
 */
public class ChatMessage {
	
	private String username;
	private String msg;
	private String time;
	private boolean mine;
	private boolean global;
	private boolean notification;
	private boolean sent;
	private String msgID;
	
	public ChatMessage(String message, boolean isNotification) {
		msg = message;
		notification = isNotification;
	}
	
	public ChatMessage(String username, String msg, String time, boolean mine, boolean global, boolean sent, String msgID) {
		this.username = username;
		this.msg = msg;
		this.time = time;
		this.mine = mine;
		this.global = global;
		this.sent = sent;
		this.msgID = msgID;
		this.notification = false;
	}
	
	public String getUsername() {
		return username;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getMsg() {
		return msg;
	}
	
	public void setMsg(String msg) {
		this.msg = msg;
	}
	
	public String getTime() {
		return time;
	}
	
	public void setTime(String time) {
		this.time = time;
	}
	
	public boolean isMine() {
		return mine;
	}
	
	public void setMine(boolean mine) {
		this.mine = mine;
	}
	
	public boolean isGlobal() {
		return global;
	}
	
	public void setGlobal(boolean global) {
		this.global = global;
	}
	
	public boolean isNotification() {
		return notification;
	}
	
	public boolean isSent() {
		return sent;
	}
	
	public void setSent(boolean sent) {
		this.sent = sent;
	}
	
	public String getMsgID() {
		return msgID;
	}
	
	public void setMsgID(String msgID) {
		this.msgID = msgID;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		
		ChatMessage chatMessage = (ChatMessage) o;
		
		return mine == chatMessage.mine &&
			username.equals(chatMessage.username) &&
			msg.equals(chatMessage.msg) &&
			time.equals(chatMessage.time);
	}
	
	@Override
	public int hashCode() {
		int result = username.hashCode();
		result = 31 * result + msg.hashCode();
		result = 31 * result + time.hashCode();
		result = 31 * result + (mine ? 1 : 0);
		return result;
	}
}