package com.suricates.neartalk;

import android.app.Activity;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.design.widget.TabLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;

import com.suricates.neartalk.tabs.fragments.GlobalChat;
import com.suricates.neartalk.tabs.ViewPagerAdapter;
import com.suricates.neartalk.tabs.fragments.InfoTab;
import com.suricates.neartalk.tabs.fragments.PreferencesFragment;
import com.suricates.neartalk.utils.Const;
import com.suricates.neartalk.utils.DBHelper;
import com.suricates.neartalk.utils.Preferences;
import com.suricates.neartalk.utils.U;

/**
 * This class is only used to load the basic layout and the tabs,
 * do tasks when the app is closed, on handle intent results
 */
public class MainActivity extends AppCompatActivity {
	
	private TabLayout tabs;
	private ViewPager viewPager;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
		
		viewPager = (ViewPager) findViewById(R.id.viewpager);
		setupViewPager(viewPager);
		viewPager.setCurrentItem(Const.tab.ZONE_CHAT);
		
		tabs = (TabLayout) findViewById(R.id.tabs);
		tabs.setupWithViewPager(viewPager);
		setupTabIcons();
	}
	
	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		viewPager.setCurrentItem(Const.tab.ZONE_CHAT);
		Preferences.set(this, Const.sp.GLOBAL_CHAT_VISIBLE, true);
	}
	
	/**
	 * Adds the tabs
	 *
	 * @param viewPager where to add the tabs
	 */
	private void setupViewPager(ViewPager viewPager) {
		ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
		adapter.addFragment(new PreferencesFragment(), null);
		adapter.addFragment(new GlobalChat(), getString(R.string.app_name).toUpperCase());
		adapter.addFragment(new InfoTab(), null);
		viewPager.setAdapter(adapter);
	}
	
	/**
	 * Sets icons to the tabs
	 */
	@SuppressWarnings("all")
	private void setupTabIcons() {
		tabs.getTabAt(Const.tab.SETTINGS).setIcon(R.drawable.ic_settings);
		tabs.getTabAt(Const.tab.ZONE_CHAT).setIcon(R.drawable.ic_launcher_white);
		tabs.getTabAt(Const.tab.LOCATION).setIcon(R.drawable.ic_world);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			switch (requestCode) {
				case Const.codes.RINGTONE:
					Uri ringtoneUri = data.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI);
					if (ringtoneUri != null) {
						Preferences.set(this, Const.sp.RINGTONE, ringtoneUri.toString());
					}
					break;
				case Const.codes.CHAT_BG:
					Uri bg = data.getData();
					if (bg != null) {
						U.sendToast(R.string.change_chat_bg_toast, this);
						Preferences.set(this, Const.sp.CHAT_BG, bg.toString());
						LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(new Intent(Const.actions.UPDATE_BACKGROUND));
					}
					break;
				default:
					super.onActivityResult(requestCode, resultCode, data);
					break;
			}
		}
	}
	
	@Override
	protected void onDestroy() {
		new DBHelper(this).deleteOldZoneMessages();
		super.onDestroy();
	}
}
